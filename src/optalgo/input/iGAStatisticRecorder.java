package optalgo.input;

import java.util.BitSet;

public interface iGAStatisticRecorder
{
    /*public void storeLaunchParams(int geneLength, int populationSize, int populationIncrease,
            int generationsCount, double mutationProbability, iSelectionMechanism parentsSelectionMechanism,
            int crossingPositions, double maxCostValue, double targetFitness, int leanGenerationsLimit, 
            double leanGenerationsDelta, int maxBadGenerationsForChromosome, int eliteCount, 
            int rouletteCount, iFitnessFunctionProvider fitnessFunctionProvider,
            iFitnessFunctionProvider costFunctionProvider);*/
    
    public void storeIterationResult(
            int iterationNumber, 
            double maxFitness, 
            double averageFitness, 
            double maxCost,
            BitSet best_genotype);
}
