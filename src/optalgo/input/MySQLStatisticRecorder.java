package optalgo.input;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.BitSet;

import optalgo.geneticalgo.SelectionType;
import optalgo.geneticalgo.iFitnessFunctionProvider;
import optalgo.simulatedannealing.iCoolingSchedule;
import tests.DTrace;

public class MySQLStatisticRecorder implements iGAStatisticRecorder
{

    public int configurationIndex;
    
    @Override
    public void storeIterationResult(int iterationNumber, double maxFitness,
            double averageFitness, double maxCost, BitSet best_genotype)
    {
        Connection conn = null;
        try
        {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/optalgo", "Qwerty", "Qwerty");
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        if (conn == null) {
            DTrace.log("DB not connected");
            System.exit(-1);
        }

        PreparedStatement stmt = null;
        try
        {
            
            int genotypeId = 0;
            
            StringBuilder bitString = new StringBuilder();
            for (int i=0; i<best_genotype.length(); i++)
            {
                if (best_genotype.get(i))
                    bitString.append("1");
                else
                    bitString.append("0");
            }
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`genotypes` (`genotype`)"
                                            +"VALUES (?);", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, bitString.toString());
            int rs = stmt.executeUpdate();
            
            if (rs == 0) {
                DTrace.log("Add genotype failed, no rows affected.");
                System.exit(-1);
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    genotypeId =  generatedKeys.getInt(1);
                }
                else {
                    DTrace.log("Add genotype failed, no ID obtained.");
                    System.exit(-1);
                }
            }
            
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`solutions`"
+"(`configuration`, `launch`, `iteration`, `best_fitness`, `best_cost`, `best_genotype`, `average_fitness`)"
+"VALUES (?,        1,        ?,           ?,              ?,           ?,               ?);");
            stmt.setInt(1, configurationIndex);
            stmt.setInt(2, iterationNumber);
            stmt.setDouble(3, maxFitness);
            stmt.setDouble(4, maxCost);
            stmt.setInt(5, genotypeId);
            stmt.setDouble(6, averageFitness);
            
            rs = stmt.executeUpdate();
            
            if (rs == 0) {
                DTrace.log("Add genotype failed, no rows affected.");
                System.exit(-1);
            }
            
            stmt.close();
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    
    public void write_GA_configuration (int geneLength,
            int populationSize,
            int populationIncrease,
            int generationsCount,
            double mutationProbability,
            SelectionType parentsSelectionWay,
            int crossingPositions,
            double maxCostValue,
            double targetFitness,
            int leanGenerationLimit,
            double leanGenerationsDelta,
            int maxBadGenerationsForChromosome,
            int eliteCount,
            int rouletteCount,
            iFitnessFunctionProvider fitFuncProvider,
            iFitnessFunctionProvider costFuncProvider)
    {
        Connection conn = null;
        int configurationId = 0;
        try
        {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/optalgo", "Qwerty", "Qwerty");
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        if (conn == null) {
            DTrace.log("DB not connected");
            System.exit(-1);
        }

        PreparedStatement stmt = null;
        try
        {
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations` (`comment`, `date_time`)"
                                          +"VALUES ('Genetic Algorithm', NOW());", Statement.RETURN_GENERATED_KEYS);
            int rs = stmt.executeUpdate(); 
            
            if (rs == 0) {
                DTrace.log("Add genotype failed, no rows affected.");
                System.exit(-1);
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    configurationId =  generatedKeys.getInt(1);
                }
                else {
                    DTrace.log("Add genotype failed, no ID obtained.");
                    System.exit(-1);
                }
            }
            
            
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations_integers` (`conf_id`, `parameter`, `value`)"
                                            +"VALUES (?, ?, ?);");
            stmt.setInt(1, configurationId);
            
            stmt.setInt(2, 1);
            stmt.setInt(3, geneLength);
            stmt.executeUpdate();
            
            stmt.setInt(2, 2);
            stmt.setInt(3, populationSize);
            stmt.executeUpdate();
            
            stmt.setInt(2, 3);
            stmt.setInt(3, populationIncrease);
            stmt.executeUpdate();
            
            stmt.setInt(2, 4);
            stmt.setInt(3, generationsCount);
            stmt.executeUpdate();
            
            stmt.setInt(2, 7);
            stmt.setInt(3, crossingPositions);
            stmt.executeUpdate();
            
            stmt.setInt(2, 10);
            stmt.setInt(3, leanGenerationLimit);
            stmt.executeUpdate();
            
            stmt.setInt(2, 12);
            stmt.setInt(3, maxBadGenerationsForChromosome);
            stmt.executeUpdate();
            
            stmt.setInt(2, 13);
            stmt.setInt(3, eliteCount);
            stmt.executeUpdate();
            
            stmt.setInt(2, 14);
            stmt.setInt(3, rouletteCount);
            stmt.executeUpdate();
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations_doubles` (`conf_id`, `parameter`, `value`)"
                    +"VALUES (?, ?, ?);");
            stmt.setInt(1, configurationId);
            
            stmt.setInt(2, 5);
            stmt.setDouble(3, mutationProbability);
            stmt.executeUpdate();
            
            stmt.setInt(2, 8);
            stmt.setDouble(3, maxCostValue);
            stmt.executeUpdate();
            
            stmt.setInt(2, 9);
            stmt.setDouble(3, targetFitness);
            stmt.executeUpdate();
            
            stmt.setInt(2, 11);
            stmt.setDouble(3, leanGenerationsDelta);
            stmt.executeUpdate();
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations_strings` (`conf_id`, `parameter`, `value`)"
                    +"VALUES (?, ?, ?);");
            stmt.setInt(1, configurationId);
            
            stmt.setInt(2, 6);
            stmt.setString(3, parentsSelectionWay.toString());
            stmt.executeUpdate();
            
            stmt.setInt(2, 15);
            stmt.setString(3, fitFuncProvider.toString());
            stmt.executeUpdate();
            
            stmt.setInt(2, 16);
            stmt.setString(3, costFuncProvider.toString());
            stmt.executeUpdate();
            
            stmt.close();
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        configurationIndex = configurationId;
    }

    public void write_SAA_configuration (int geneLength,
            int generationsCount,
            double maxCostValue,
            double targetFitness,
            int startTemperature,
            int endTemperature,
            int stepsCount,
            iFitnessFunctionProvider fitFuncProvider,
            iFitnessFunctionProvider costFuncProvider,
            iCoolingSchedule coolingShedule)
    {
        Connection conn = null;
        int configurationId = 0;
        try
        {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/optalgo", "Qwerty", "Qwerty");
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        
        if (conn == null) {
            DTrace.log("DB not connected");
            System.exit(-1);
        }

        PreparedStatement stmt = null;
        try
        {
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations` (`comment`, `date_time`)"
                                          +"VALUES ('Simulated annealing Algorithm', NOW());", Statement.RETURN_GENERATED_KEYS);
            int rs = stmt.executeUpdate();
            
            if (rs == 0) {
                DTrace.log("Add genotype failed, no rows affected.");
                System.exit(-1);
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    configurationId =  generatedKeys.getInt(1);
                }
                else {
                    DTrace.log("Add genotype failed, no ID obtained.");
                    System.exit(-1);
                }
            }
            
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations_integers` (`conf_id`, `parameter`, `value`)"
                                            +"VALUES (?, ?, ?);");
            stmt.setInt(1, configurationId);
            
            stmt.setInt(2, 1);
            stmt.setInt(3, geneLength);
            stmt.executeUpdate();
            
            stmt.setInt(2, 4);
            stmt.setInt(3, generationsCount);
            stmt.executeUpdate();
            
            stmt.setInt(2, 17);
            stmt.setInt(3, startTemperature);
            stmt.executeUpdate();
            
            stmt.setInt(2, 18);
            stmt.setInt(3, endTemperature);
            stmt.executeUpdate();
            
            stmt.setInt(2, 19);
            stmt.setInt(3, stepsCount);
            stmt.executeUpdate();
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations_doubles` (`conf_id`, `parameter`, `value`)"
                    +"VALUES (?, ?, ?);");
            stmt.setInt(1, configurationId);
            
            stmt.setInt(2, 8);
            stmt.setDouble(3, maxCostValue);
            stmt.executeUpdate();
            
            stmt.setInt(2, 9);
            stmt.setDouble(3, targetFitness);
            stmt.executeUpdate();
            
            stmt = conn.prepareStatement("INSERT INTO `optalgo`.`configurations_strings` (`conf_id`, `parameter`, `value`)"
                    +"VALUES (?, ?, ?);");
            stmt.setInt(1, configurationId);
            
            stmt.setInt(2, 15);
            stmt.setString(3, fitFuncProvider.toString());
            stmt.executeUpdate();
            
            stmt.setInt(2, 16);
            stmt.setString(3, costFuncProvider.toString());
            stmt.executeUpdate();
            
            stmt.setInt(2, 20);
            stmt.setString(3, coolingShedule.toString());
            stmt.executeUpdate();
            
            stmt.close();
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        configurationIndex = configurationId;
    }
}
