package optalgo.input;

import optalgo.fitnessfunctions.GraphCost;
import optalgo.fitnessfunctions.MooreShannon;
import optalgo.fitnessfunctions.StoerWagner;
import optalgo.geneticalgo.SelectionType;
import optalgo.geneticalgo.iFitnessFunctionProvider;
import optalgo.simulatedannealing.FirstSchedule;
import optalgo.simulatedannealing.FourthSchedule;
import optalgo.simulatedannealing.SecondSchedule;
import optalgo.simulatedannealing.ThirdSchedule;
import optalgo.simulatedannealing.iCoolingSchedule;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import tests.DTrace;

import java.io.*;

import javax.xml.parsers.*;

public class XmlGaLaunchConfigurationLoader
{
    private String algorithmName;
    
    //GA
    private int geneLength_from, geneLength_to, geneLength_step;
    private int populationSize_from, populationSize_to, populationSize_step;
    private int populationIncrease_from, populationIncrease_to, populationIncrease_step;
    private int generationsCount_from, generationsCount_to, generationsCount_step;
    private double mutationProbability_from, mutationProbability_to, mutationProbability_step;
    private SelectionType parentsSelectionWay;
    private int crossingPositions_from, crossingPositions_to, crossingPositions_step;
    private double maxCostValue_from, maxCostValue_to, maxCostValue_step;
    private double targetFitness_from, targetFitness_to, targetFitness_step;
    private int leanGenerationsLimit_from, leanGenerationsLimit_to, leanGenerationsLimit_step;
    private double leanGenerationsDelta_from, leanGenerationsDelta_to, leanGenerationsDelta_step;
    private int maxBadGenerationsForChromosome_from, maxBadGenerationsForChromosome_to, maxBadGenerationsForChromosome_step;
    private int eliteCount_from, eliteCount_to, eliteCount_step;
    private int rouletteCount_from, rouletteCount_to, rouletteCount_step;
    private iFitnessFunctionProvider fitnessFunctionProvider;
    private iFitnessFunctionProvider costFunctionProvider;
    
    //SAA
    private int startTemperature_from, startTemperature_to, startTemperature_step;
    private int endTemperature_from, endTemperature_to, endTemperature_step;
    private int stepsCount_from, stepsCount_to, stepsCount_step;
    private iCoolingSchedule coolingSchedule;
    

    private boolean m_initialised;
    private String m_launchConfigurationId;

    public void loadXml(String configurationId) throws Exception, IOException
    {
        DTrace.in();
        m_launchConfigurationId = configurationId;
        Exception ex = new Exception("XML format is invalid");

        File xmlf = new File("c:\\ga\\" + configurationId + ".xml");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(xmlf);

        Element root = doc.getDocumentElement();

        if (!("algorithm".equals(root.getTagName())))
        {
            throw ex;
        }
        
        algorithmName = root.getAttribute("name");
        
        NodeList params = root.getChildNodes();
        for (int i = 0; i < params.getLength(); i++)
        {
            Node param = params.item(i);
            if (param instanceof Element)
            {
                if ((((Element) param).getTagName().equals("launchparam")))
                    parseParam(param);
            }
        }

        m_initialised = true;
        //DTrace.log("initialised="+m_initialised);
        DTrace.out();
    }

    private void parseParam(Node param)
    {
        Element el = (Element) param;
        String name = el.getAttribute("name");
        
        if (el.hasAttribute("set"))
        {
            String set = el.getAttribute("set");
            switch (name)
            {
            case "parentsSelectionMechanism":
                switch (set)
                {
                case "Random":
                case "ST_RANDOM":
                    parentsSelectionWay = SelectionType.ST_RANDOM;
                    break;
                    
                case "Roulette":
                case "ST_ROULETTE":
                    parentsSelectionWay = SelectionType.ST_ROULETTE;
                    break;
                    
                case "InbreedingRoulette":
                case "ST_INBREEDING_ROULETTE":
                    parentsSelectionWay = SelectionType.ST_INBREEDING_ROULETTE;
                    break;
                    
                case "InbreedingRandom":
                case "ST_INBREEDING_RANDOM":
                    parentsSelectionWay = SelectionType.ST_INBREEDING_RANDOM;
                    break;
                    
                case "OutbreedingRoulette":
                case "ST_OUTBREEDING_ROULETTE":
                    parentsSelectionWay = SelectionType.ST_OUTBREEDING_ROULETTE;
                    break;
                    
                case "OutbreedingRandom":
                case "ST_OUTBREEDING_RANDOM":
                    parentsSelectionWay = SelectionType.ST_OUTBREEDING_RANDOM;
                    break;
                    
                case "Elite":
                case "ST_ELITE":
                    parentsSelectionWay = SelectionType.ST_ELITE;
                    break;
                }
                break;
            case "fitnessFunctionProvider":
                switch (set)
                {
                case "MooreShannon":
                    fitnessFunctionProvider = new MooreShannon();
                    break;
                case "StoerWagner":
                    fitnessFunctionProvider = new StoerWagner();
                    break;
                }
                break;
            case "costFunctionProvider":
                switch (set)
                {
                case "GraphCost":
                    costFunctionProvider = new GraphCost();
                    break;
                }
                break;
            case "coolingSchedule":
                switch (set)
                {
                case "First":
                    coolingSchedule = new FirstSchedule();
                    break;
                case "Second":
                    coolingSchedule = new SecondSchedule();
                    break;
                case "Third":
                    coolingSchedule = new ThirdSchedule();
                    break;
                case "Fourth":
                    coolingSchedule = new FourthSchedule();
                    break;
                }
                break;
            }
        }
        
        if (el.hasAttribute("from"))
        {
            String from = el.getAttribute("from");
            String to = el.getAttribute("to");
            String step = el.getAttribute("step");
            
            switch (name)
            {
            case "geneLength":
                geneLength_from = Integer.parseInt(from);
                geneLength_to = Integer.parseInt(to);
                geneLength_step = Integer.parseInt(step);
                
                break;
    
            case "populationSize":
                populationSize_from = Integer.parseInt(from);
                populationSize_to = Integer.parseInt(to);
                populationSize_step = Integer.parseInt(step);
                break;
    
            case "populationIncrease":
                populationIncrease_from = Integer.parseInt(from);
                populationIncrease_to = Integer.parseInt(to);
                populationIncrease_step = Integer.parseInt(step);
                break;
    
            case "generationsCount":
                generationsCount_from = Integer.parseInt(from);
                generationsCount_to = Integer.parseInt(to);
                generationsCount_step = Integer.parseInt(step);
                break;
                
            case "mutationProbability":
                mutationProbability_from = Double.parseDouble(from);
                mutationProbability_to = Double.parseDouble(to);
                mutationProbability_step = Double.parseDouble(step);
                break;
                
            case "crossingPositions":
                crossingPositions_from = Integer.parseInt(from);
                crossingPositions_to = Integer.parseInt(to);
                crossingPositions_step = Integer.parseInt(step);
                break;
                
            case "maxCostValue":
                maxCostValue_from = Double.parseDouble(from);
                maxCostValue_to = Double.parseDouble(to);
                maxCostValue_step = Double.parseDouble(step);
                break;
                
            case "targetFitness":
                targetFitness_from = Double.parseDouble(from);
                targetFitness_to = Double.parseDouble(to);
                targetFitness_step = Double.parseDouble(step);
                break;
                
            case "leanGenerationsLimit":
                leanGenerationsLimit_from = Integer.parseInt(from);
                leanGenerationsLimit_to = Integer.parseInt(to);
                leanGenerationsLimit_step = Integer.parseInt(step);
                break;
                
            case "leanGenerationsDelta":
                leanGenerationsDelta_from = Double.parseDouble(from);
                leanGenerationsDelta_to = Double.parseDouble(to);
                leanGenerationsDelta_step = Double.parseDouble(step);
                break;
                
            case "maxBadGenerationsForChromosome":
                maxBadGenerationsForChromosome_from = Integer.parseInt(from);
                maxBadGenerationsForChromosome_to = Integer.parseInt(to);
                maxBadGenerationsForChromosome_step = Integer.parseInt(step);
                break;
                
            case "eliteCount":
                eliteCount_from = Integer.parseInt(from);
                eliteCount_to = Integer.parseInt(to);
                eliteCount_step = Integer.parseInt(step);
                break;
                
            case "rouletteCount":
                rouletteCount_from = Integer.parseInt(from);
                rouletteCount_to = Integer.parseInt(to);
                rouletteCount_step = Integer.parseInt(step);
                break;
                
            case "startTemperature":
                startTemperature_from = Integer.parseInt(from);
                startTemperature_to = Integer.parseInt(to);
                startTemperature_step = Integer.parseInt(step);
                break;
                
            case "endTemperature":
                endTemperature_from = Integer.parseInt(from);
                endTemperature_to = Integer.parseInt(to);
                endTemperature_step = Integer.parseInt(step);
                break;
                
            case "stepsCount":
                stepsCount_from = Integer.parseInt(from);
                stepsCount_to = Integer.parseInt(to);
                stepsCount_step = Integer.parseInt(step);
                break;
                
            }
        }
    }

    public boolean isInitialised()
    {
        return m_initialised;
    }

    public int getGeneLength()
    {
        return geneLength_from;
    }

    public int getPopulationSize()
    {
        return populationSize_from;
    }

    public int getPopulationIncrease()
    {
        return populationIncrease_from;
    }

    public int getGenerationsCount()
    {
        return generationsCount_from;
    }

    public double getMutationProbability()
    {
        return mutationProbability_from;
    }

    public SelectionType getParentsSelectionWay()
    {
        return parentsSelectionWay;
    }

    public int getCrossingPositions()
    {
        return crossingPositions_from;
    }

    public double getMaxCostValue()
    {
        return maxCostValue_from;
    }

    public double getTargetFitness()
    {
        return targetFitness_from;
    }

    public int getLeanGenerationsLimit()
    {
        return leanGenerationsLimit_from;
    }

    public double getLeanGenerationsDelta()
    {
        return leanGenerationsDelta_from;
    }

    public int getMaxBadGenerationsForChromosome()
    {
        return maxBadGenerationsForChromosome_from;
    }

    public int getEliteCount()
    {
        return eliteCount_from;
    }

    public int getRouletteCount()
    {
        return rouletteCount_from;
    }

    public iFitnessFunctionProvider getFitnessFunctionProvider()
    {
        return fitnessFunctionProvider;
    }

    public iFitnessFunctionProvider getCostFunctionProvider()
    {
        return costFunctionProvider;
    }
    
    
    
    //Autogenerated getters
    public int getGeneLength_from()
    {
        return geneLength_from;
    }

    public int getGeneLength_to()
    {
        return geneLength_to;
    }

    public int getGeneLength_step()
    {
        return geneLength_step;
    }

    public int getPopulationSize_from()
    {
        return populationSize_from;
    }

    public int getPopulationSize_to()
    {
        return populationSize_to;
    }

    public int getPopulationSize_step()
    {
        return populationSize_step;
    }

    public int getPopulationIncrease_from()
    {
        return populationIncrease_from;
    }

    public int getPopulationIncrease_to()
    {
        return populationIncrease_to;
    }

    public int getPopulationIncrease_step()
    {
        return populationIncrease_step;
    }

    public int getGenerationsCount_from()
    {
        return generationsCount_from;
    }

    public int getGenerationsCount_to()
    {
        return generationsCount_to;
    }

    public int getGenerationsCount_step()
    {
        return generationsCount_step;
    }

    public double getMutationProbability_from()
    {
        return mutationProbability_from;
    }

    public double getMutationProbability_to()
    {
        return mutationProbability_to;
    }

    public double getMutationProbability_step()
    {
        return mutationProbability_step;
    }

    public int getCrossingPositions_from()
    {
        return crossingPositions_from;
    }

    public int getCrossingPositions_to()
    {
        return crossingPositions_to;
    }

    public int getCrossingPositions_step()
    {
        return crossingPositions_step;
    }

    public double getMaxCostValue_from()
    {
        return maxCostValue_from;
    }

    public double getMaxCostValue_to()
    {
        return maxCostValue_to;
    }

    public double getMaxCostValue_step()
    {
        return maxCostValue_step;
    }

    public double getTargetFitness_from()
    {
        return targetFitness_from;
    }

    public double getTargetFitness_to()
    {
        return targetFitness_to;
    }

    public double getTargetFitness_step()
    {
        return targetFitness_step;
    }

    public int getLeanGenerationsLimit_from()
    {
        return leanGenerationsLimit_from;
    }

    public int getLeanGenerationsLimit_to()
    {
        return leanGenerationsLimit_to;
    }

    public int getLeanGenerationsLimit_step()
    {
        return leanGenerationsLimit_step;
    }

    public double getLeanGenerationsDelta_from()
    {
        return leanGenerationsDelta_from;
    }

    public double getLeanGenerationsDelta_to()
    {
        return leanGenerationsDelta_to;
    }

    public double getLeanGenerationsDelta_step()
    {
        return leanGenerationsDelta_step;
    }

    public int getMaxBadGenerationsForChromosome_from()
    {
        return maxBadGenerationsForChromosome_from;
    }

    public int getMaxBadGenerationsForChromosome_to()
    {
        return maxBadGenerationsForChromosome_to;
    }

    public int getMaxBadGenerationsForChromosome_step()
    {
        return maxBadGenerationsForChromosome_step;
    }

    public int getEliteCount_from()
    {
        return eliteCount_from;
    }

    public int getEliteCount_to()
    {
        return eliteCount_to;
    }

    public int getEliteCount_step()
    {
        return eliteCount_step;
    }

    public int getRouletteCount_from()
    {
        return rouletteCount_from;
    }

    public int getRouletteCount_to()
    {
        return rouletteCount_to;
    }

    public int getRouletteCount_step()
    {
        return rouletteCount_step;
    }

    /**
     * @return the algorithmName
     */
    public String getAlgorithmName()
    {
        return algorithmName;
    }

    /**
     * @return the startTemperature_from
     */
    public int getStartTemperature_from()
    {
        return startTemperature_from;
    }

    /**
     * @return the startTemperature_to
     */
    public int getStartTemperature_to()
    {
        return startTemperature_to;
    }

    /**
     * @return the startTemperature_step
     */
    public int getStartTemperature_step()
    {
        return startTemperature_step;
    }

    /**
     * @return the endTemperature_from
     */
    public int getEndTemperature_from()
    {
        return endTemperature_from;
    }

    /**
     * @return the endTemperature_to
     */
    public int getEndTemperature_to()
    {
        return endTemperature_to;
    }

    /**
     * @return the endTemperature_step
     */
    public int getEndTemperature_step()
    {
        return endTemperature_step;
    }

    /**
     * @return the stepsCount_from
     */
    public int getStepsCount_from()
    {
        return stepsCount_from;
    }

    /**
     * @return the stepsCount_to
     */
    public int getStepsCount_to()
    {
        return stepsCount_to;
    }

    /**
     * @return the stepsCount_step
     */
    public int getStepsCount_step()
    {
        return stepsCount_step;
    }

    /**
     * @return the coolingSchedule
     */
    public iCoolingSchedule getCoolingSchedule()
    {
        return coolingSchedule;
    }

}
