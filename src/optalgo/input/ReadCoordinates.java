package optalgo.input;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;



public class ReadCoordinates
{

    public City[] ReadFile(int file_number)
    {
        String s = "";
        Scanner in = null;

            //String uri = System.getProperty("user.dir")+"\\msk\\msk_"+file_number+".json";
            String uri = "c:\\ga\\msk_" + file_number + ".json";
            File f = new File (uri);
            try
            {
                in = new Scanner(f);
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            s += in.nextLine();

        in.close();
        Gson gson = new Gson();  
        City[] data = gson.fromJson(s, City[].class);
        return data;
    } //verified
    
}
