package optalgo.input;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.BitSet;
import java.util.Locale;

import tests.DTrace;

public class CsvStatisticRecorder implements iGAStatisticRecorder
{
    private PrintWriter m_csv;
    private boolean m_initialised;
    private String comment;
    
    public CsvStatisticRecorder(String launchConfigurationId)
    {
        DTrace.in();
        try
        {
            m_csv = new PrintWriter(new File("c:\\ga\\"+launchConfigurationId+".csv"), "UTF-8");
            m_initialised = true;
            m_csv.println("iteration;maxfitness;averagefitness;bestcost");
        } catch (IOException e)
        {
            m_initialised = false;
            e.printStackTrace();
        }
        //DTrace.log("initialised="+m_initialised);
        DTrace.out();
    }

    @Override
    public void storeIterationResult(int iterationNumber, double maxFitness,
            double averageFitness, double maxCost, BitSet best_genotype) //TODO fix bitset saving
    {
        DTrace.in();
        if (!m_initialised)
        {
            DTrace.log("Err not initialised");
            return;
        }
        
        m_csv.printf(new Locale("ru"), "%1$d;%2$f;%3$f;%4$f", iterationNumber, maxFitness, averageFitness, maxCost);
        if (null != comment)
        {
            m_csv.print(";" + comment);
            comment = null;
        }
        m_csv.println();
        m_csv.flush();
        DTrace.out();
    }
    
    public void setComment(String msg)
    {
        if (null == comment)
            comment = msg;
        else
            comment += msg;
    }
    
    public void save()
    {
        DTrace.in();
        m_csv.flush();
        m_csv.close();
        DTrace.out();
    }
    
}
