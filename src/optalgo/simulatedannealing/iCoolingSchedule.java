package optalgo.simulatedannealing;

public interface iCoolingSchedule
{
    public double getTemperature (int step, int from, int to, int n);
    
    public String toString();
}
