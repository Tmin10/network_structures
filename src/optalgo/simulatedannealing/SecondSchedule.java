package optalgo.simulatedannealing;

public class SecondSchedule implements iCoolingSchedule
{
 
    @Override
    public String toString()
    {
        return "SecondSchedule";
    }

    @Override
    public double getTemperature(int step, int from, int to, int n)
    {
        return from*Math.pow((to/from), (step/n));
    }
}
