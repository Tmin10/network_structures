package optalgo.simulatedannealing;

public class ThirdSchedule implements iCoolingSchedule
{

    @Override
    public String toString()
    {
        return "ThirdSchedule";
    }

    @Override
    public double getTemperature(int step, int from, int to, int n)
    {
        return (((from-to)*(n+1)/n)/(step+1))+from-((from-to)*(n+1)/n);
    }
}
