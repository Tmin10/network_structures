package optalgo.simulatedannealing;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;

import optalgo.fitnessfunctions.MooreShannon;
import optalgo.geneticalgo.Chromosome;
import optalgo.geneticalgo.GaUtils;
import optalgo.geneticalgo.SelectionType;
import optalgo.geneticalgo.edge;
import optalgo.geneticalgo.iFitnessFunctionProvider;
import optalgo.geneticalgo.iItemsProvider;
import optalgo.input.iGAStatisticRecorder;
import tests.DTrace;

public class SimulatedAnnealingAlgorithm implements iItemsProvider
{
    private int m_geneLength;           // длина гена
    private int m_generationsCount;     // количество поколений (-1 - не
                                        // используется)
    private double m_maxCostValue;      // предельное значение стоимости
    private double m_targetFitness;     // fitnessValue решения должно быть не меньше
    private iFitnessFunctionProvider m_fitFuncProvider;     // интерфейс для
                                                            // предоставления
                                                            // целевой функции
    private iFitnessFunctionProvider m_costFuncProvider;    // функция ограничения
    private Random m_generator;
    private iGAStatisticRecorder m_statisticRecorder;
    private iCoolingSchedule m_CoolingSchedule;
    private int m_iterations;
    private int m_generations;
    private int m_stepsCount;
    private double m_currentTemperature;
    private Chromosome m_currentChromosome;
    private Chromosome m_nextChromosome;
    private double m_currentFitness;
    private double m_nextFitness;
    private BitSet m_globalBestGenotype;
    private double m_globalBestFitness;
    private double m_globalBestCost;
    private int m_startTemperature;
    private int m_endTemperature;
    private double[][] m_costs;
    
    
    public SimulatedAnnealingAlgorithm(int geneLength,
            int generationsCount,
            double maxCostValue,
            double targetFitness,
            int startTemperature,
            int endTemperature,
            int stepsCount,
            iFitnessFunctionProvider fitFuncProvider,
            iFitnessFunctionProvider costFuncProvider,
            iGAStatisticRecorder statisticRecorder,
            iCoolingSchedule coolingSchedule)
    {
        m_geneLength = geneLength;
        m_iterations = 0;
        m_generations = 0;
        m_stepsCount = stepsCount;
        m_generationsCount = generationsCount;
        
        if (maxCostValue > 1 || maxCostValue < 0)
        {
            DTrace.log("Incorrect maxCostValue " + maxCostValue);
            System.exit(-1);
        }
        m_costFuncProvider = costFuncProvider;
        double fullNetCost =  calculateFullNetCost();
        m_maxCostValue = fullNetCost * maxCostValue;
        
        DTrace.log("Cost: "+m_maxCostValue+"/"+fullNetCost);
        
        m_globalBestCost = Double.MAX_VALUE;
        
        if (targetFitness > 1 || targetFitness < 0)
        {
            DTrace.log("Incorrect targetFitness " + targetFitness);
            System.exit(-1);
        }
        m_costs = getCosts();
        m_startTemperature = startTemperature;
        m_endTemperature = endTemperature;
        m_targetFitness = targetFitness;
        
        m_fitFuncProvider = fitFuncProvider;
        m_CoolingSchedule = coolingSchedule;
        m_statisticRecorder = statisticRecorder;
        m_generator = new Random();
        m_currentChromosome = new Chromosome();
        m_currentChromosome.length = m_geneLength;
        m_nextChromosome = new Chromosome();
        m_nextChromosome.length = m_geneLength;
        initChromosome(m_currentChromosome);
        
    }
    
    //Copy from GA
    private void initChromosome(Chromosome c)
    {
        ArrayList<Integer> vertices = new ArrayList<Integer>();
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);

        for (int i = 1; i <= servers; i++)
        {
            vertices.add(i);
        }

        int prev = 0, next;
        int nextIdx;
        do
        {
            nextIdx = m_generator.nextInt(vertices.size());
            next = vertices.get(nextIdx);
            vertices.remove(nextIdx);

            if (prev > 0)
            {
                setConnect(c, prev, next);
            }

            prev = next;
        } while (vertices.size() > 0);
    }
    
    //Copy from GA
    private void setConnect(Chromosome c, int x, int y) // x,y с единицы
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        if (y > x)
        {
            x += y;
            y = x - y;
            x -= y;
        }
        int offset = (int) ((y - 1) * servers - (y - 1) * (1 + (y - 2) * 0.5))
                + (x - y - 1);
        c.genotype.set(offset);
    }
    
    //Copy from GA
    private double[][] getCosts()
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        double ret[][] = new double[servers][servers];
        Chromosome c = new Chromosome();
        c.length = m_geneLength;
        for (int i = 1; i < servers; i++)
        {
            for (int j = i + 1; j <= servers; j++)
            {
                c.genotype.clear();
                setConnect(c, i, j);
                ret[i - 1][j - 1] = m_costFuncProvider.check(c);
                ret[j - 1][i - 1] = ret[i - 1][j - 1];
            }
            ret[i - 1][i - 1] = 0;
        }
        return ret;
    }


    private double calculateFullNetCost()
    {
        Chromosome c = new Chromosome();
        c.length = m_geneLength;
        c.genotype.set(0, c.length - 1);
        return m_costFuncProvider.check(c);
    }


    @Override
    public Chromosome[] getItems()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getAvailableItemsCount()
    {
        // TODO Auto-generated method stub
        return 0;
    }
    
    public void run()
    {
        m_currentTemperature = m_CoolingSchedule.getTemperature(m_iterations, m_startTemperature, m_endTemperature, m_stepsCount);
        m_currentFitness = getFitness(m_currentChromosome);
        m_globalBestGenotype = (BitSet) m_currentChromosome.genotype.clone();
        while (true)
        {
            generateNextChromosome();
            m_nextFitness = getFitness(m_nextChromosome);
            double bad_choise_probability = Math.pow(Math.E, ((m_currentFitness-m_nextFitness)/m_currentTemperature));
            if (m_nextFitness>m_currentFitness||m_generator.nextDouble()<bad_choise_probability)
            {
                m_currentChromosome.genotype = (BitSet) m_nextChromosome.genotype.clone();
                m_currentFitness = m_nextFitness;
                updateGlobalBestResult();
                m_iterations++;
                m_currentTemperature = m_CoolingSchedule.getTemperature(m_iterations, m_startTemperature, m_endTemperature, m_stepsCount);
            }
            if (stopByTemperature()||stopByFitness()||stopByGenerations())
            {
                break;
            }
            storeIterationResults();
            m_generations++;
            
        }
    }
    
    private void storeIterationResults()
    {
        m_statisticRecorder.storeIterationResult(m_generations, m_globalBestFitness, m_currentFitness, m_globalBestCost, m_globalBestGenotype);
    }

    private boolean stopByGenerations()
    {
        if (m_generations>=m_generationsCount)
        {
            DTrace.log("stopByGenerations");
            return true;
        }
        return false;
    }

    private boolean stopByFitness()
    {
        if (m_globalBestFitness>=m_targetFitness)
        {
            DTrace.log("stopByFitness");
            return true;
        }
        return false;
    }

    private boolean stopByTemperature()
    {
        if (m_currentTemperature<=m_endTemperature)
        {
            DTrace.log("stopByTemperature");
            return true;
        }
        return false;
    }

    private void updateGlobalBestResult()
    {
        if (m_globalBestFitness<m_currentFitness)
        {
            m_globalBestFitness = m_currentFitness;
            m_globalBestGenotype = (BitSet) m_currentChromosome.genotype.clone();
            m_globalBestCost = m_costFuncProvider.check(m_currentChromosome);
        }
    }

    private double getFitness(Chromosome c)
    {
        return m_fitFuncProvider.check(c);
    }

    private void generateNextChromosome()
    {
        Chromosome nextChromosome = m_nextChromosome;
        nextChromosome.genotype = (BitSet) m_currentChromosome.genotype.clone();
        
        double mutation_probability = (m_currentTemperature*0.1)/(m_startTemperature-m_endTemperature);
        DTrace.log("mutation: "+mutation_probability);
        double chomosomeCost;
        do
        {
            for (int i = 0; i < m_geneLength; i++)
            {
                if (m_generator.nextDouble() <mutation_probability)
                {
                    nextChromosome.genotype.flip(i);
                }
            }
            correctChromosome(nextChromosome);
            chomosomeCost = m_costFuncProvider.check(nextChromosome);
            //DTrace.log("Chromosome cost: "+chomosomeCost+"/"+m_maxCostValue);
        } while (chomosomeCost>=m_maxCostValue);
        
    }
    
    //Copy from GA
    private void correctChromosome(Chromosome c)
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        ArrayList<Integer> vertices = MooreShannon
                .getServersConnectedWith(1, c);
        edge newConnection;

        while (vertices.size() < servers)
        {
            newConnection = getCheapServerIndex(vertices, servers);
            ArrayList<Integer> secondPart = MooreShannon
                    .getServersConnectedWith(newConnection.toIdx, c);
            setConnect(c, newConnection.fromIdx, newConnection.toIdx);
            vertices.addAll(secondPart);
        }
        decreaseChromosomeCost(c);
    }
    
   //Copy from GA
    private void decreaseChromosomeCost(Chromosome c)
    {
        if (m_maxCostValue - c.costValue > 1e-7)
            return;
        
        ArrayList<Integer> removedEdgesIdxs = new ArrayList<Integer>();
        int removed;
        
        do
        {
            removed = removeMostExpensiveEdge(c);
            correctChromosome(c);
            c.costValue = m_costFuncProvider.check(c);

            if (m_maxCostValue - c.costValue > 1e-7)
                return;
        } while (!removedEdgesIdxs.contains(removed));
        
        c.genotype.clear();
        initChromosome(c);
        c.costValue = m_costFuncProvider.check(c);
    }
    
    //Copy from GA
    private int removeMostExpensiveEdge(Chromosome c)
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        int from = 0, to = 0;
        double maxCost = 0;
        int maxCostIdx = -1;
        
        for (int i = 0; i < c.length; i++)
        {
            from = MooreShannon.get_x(servers, i);
            to = MooreShannon.get_y(servers, i);
            
            if (m_costs[from - 1][to - 1] > maxCost)
            {
                maxCost = m_costs[from - 1][to - 1];
                maxCostIdx = i;
            }
        }
        
        c.genotype.clear(maxCostIdx);
        return maxCostIdx;
    }

    //Copy from GA
    public edge getCheapServerIndex(ArrayList<Integer> vertices,
            int serversCount)
    {
        double minCost = Double.MAX_VALUE;
        int fromIdx = 0, toIdx = 0;
        boolean firstIter = true;

        for (int i = 0; i < vertices.size(); i++)
        {
            fromIdx = vertices.get(i);
            for (int j = 1; j <= serversCount; j++)
            {
                if (!vertices.contains(j))
                {
                    if (firstIter)
                    {
                        firstIter = false;
                        minCost = m_costs[j - 1][fromIdx - 1];
                        toIdx = j;
                    } else if (m_costs[j - 1][fromIdx - 1] < minCost)
                    {
                        minCost = m_costs[j - 1][fromIdx - 1];
                        toIdx = j;
                    }
                }
            }
        }
        return new edge(fromIdx, toIdx);
    }
}
