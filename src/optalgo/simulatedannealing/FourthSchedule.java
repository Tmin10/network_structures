package optalgo.simulatedannealing;

public class FourthSchedule implements iCoolingSchedule
{

    @Override
    public String toString()
    {
        return "FourthSchedule";
    }

    @Override
    public double getTemperature(int step, int from, int to, int n)
    {
        return 0.5*(from-to)*(1+Math.cos(step*Math.PI/n))+to;
    }

}
