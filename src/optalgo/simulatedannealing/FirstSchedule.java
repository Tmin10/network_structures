package optalgo.simulatedannealing;

public class FirstSchedule implements iCoolingSchedule
{
    
    @Override
    public String toString()
    {
        return "FirstSchedule";
    }

    @Override
    public double getTemperature(int step, int from, int to, int n)
    {
        return from-step*((from-to)/n);
    }

}
