package optalgo.worker;
import optalgo.geneticalgo.SelectionType;

public class SelectionTypeTask extends TaskChainItem
{
    private intTask m_iterator;
    
    public SelectionTypeTask()
    {
        try
        {
            m_iterator = new intTask(1, 7, 1);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void reset()
    {
        m_iterator.reset();
    }
    
    @Override
    public void tick()
    {
        m_iterator.tick();
        
        if (m_next != null && m_iterator.isOverflowed())
        {
            m_next.tick();
        }
    }
    
    public SelectionType get()
    {
        int current = m_iterator.get();
        
        switch (current)
        {
        case 1:
            return SelectionType.ST_ROULETTE;
        case 2:
            return SelectionType.ST_RANDOM;
        case 3:
            return SelectionType.ST_INBREEDING_ROULETTE;
        case 4:
            return SelectionType.ST_INBREEDING_RANDOM;
        case 5:
            return SelectionType.ST_OUTBREEDING_ROULETTE;
        case 6:
            return SelectionType.ST_OUTBREEDING_RANDOM;
        case 7:
            return SelectionType.ST_ELITE;
        default:
            return SelectionType.ST_RANDOM;
        }
    }
    

    @Override
    public boolean isOverflowed()
    {
        return m_iterator.isOverflowed();
    }

    @Override
    public int getIterationsCount()
    {
        int iterationsCount = 7;
        if (m_next != null)
            iterationsCount*=m_next.getIterationsCount();
        return iterationsCount;
    }

}
