package optalgo.worker;

public abstract class TaskChainItem
{
    protected TaskChainItem m_next;
    
    public TaskChainItem()
    {
        m_next = null;
    }
    
    public void connectNext(TaskChainItem next)
    {
        m_next = next;
    }
    
    public boolean isEndReached()
    {
        if (m_next == null)
        {
            return isOverflowed();
        }
        else
        {
            return m_next.isEndReached();
        }
    }
    
    public abstract void tick();
    
    public abstract boolean isOverflowed();
    
    public abstract int getIterationsCount();
}
