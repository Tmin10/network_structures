package optalgo.worker;

public class doubleTask extends TaskChainItem
{

    private double m_start;
    private double m_end;
    private double m_increase;
    
    private double m_current;
    private boolean m_overflowed;
    
    public doubleTask()
    {
        try
        {
            set(0, 0, 0);
        } catch (Exception e)
        {
            //Nothing can happens there
        } 
    }
    
    public doubleTask(double start, double end, double increase) throws Exception
    {
        set(start, end, increase);
    }
    
    public void set(double start, double end, double increase) throws Exception
    {
        if ((start < end && increase <= 0) || (start > end))
            throw new Exception("Illegal arguments passed into task.");

        m_start = start;
        m_end = end;
        m_increase = increase;
        
        m_current = m_start;
        m_overflowed = false;
    }
    
    public void reset()
    {
        m_current = m_start;
        m_overflowed = false;
    }
    
    public double get()
    {
        return m_current;
    }
    
    @Override
    public boolean isOverflowed()
    {
        return m_overflowed;
    }

    @Override
    public void tick()
    {        
        if (m_current == m_start)
        {
            m_overflowed = false;
        }
        
        m_current += m_increase;        
        
        if (m_current > m_end || m_increase==0)
        {
            m_overflowed = true;
            m_current = m_start;
            if (m_next != null)
            {
                m_next.tick();
            }
        }
    }

    @Override
    public int getIterationsCount()
    {
        int iterationsCount = 1;
        if (m_increase!=0)
            iterationsCount = (int) ((m_end-m_start)/m_increase + 1);
        if (m_next != null)
            iterationsCount*=m_next.getIterationsCount();
        return iterationsCount;
    }

}
