package optalgo.fitnessfunctions;

import optalgo.geneticalgo.Chromosome;
import optalgo.geneticalgo.iFitnessFunctionProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

import src.faketests.DTrace;

public class MooreShannon implements iFitnessFunctionProvider
{
    @Override
    public String toString()
    {
        return "MooreShannon";
    }
    
    
    @Override
    public double check(Chromosome chromosome)
    {
        DTrace.in();
        int connections = chromosome.length;
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * connections)) / 2);
        double p = 0.9; //TODO different reliabilities
        double[] arr = new double[connections];
        for (int i=0; i<connections; i++)
        {
            if (chromosome.genotype.get(i))
                arr[i]=p;
            else
                arr[i]=0;
        }
        double ret = (double)get_connectivity(arr, servers);
        DTrace.out();
        return ret;
    }
    
    private static boolean is_connectivity(double[] arr, int count)
    {
        byte [] marks = is_connectivity_b(arr, count, 1);
        for (int j=0; j<count; j++)
        {
            if (marks[j]==0)
            {
                return false;
            }
        }
        return true;
    }
    
    public static ArrayList<Integer> getServersConnectedWith(int serverIdx, Chromosome c) //serverIdx from 1
    {
        ArrayList<Integer> vertices = new ArrayList<Integer>();
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * c.length)) / 2);
        double []arr = new double[c.length];
        for (int i = 0; i < c.length; i++)
            if (c.genotype.get(i))
                arr[i] = 1.0;
        
        byte [] marks = is_connectivity_b(arr, servers, serverIdx);
        for (int i = 0; i < marks.length; i++)
        {
            if (marks[i] > 0)
                vertices.add(i + 1);
        }
        
        return vertices;
    } //verified
    
    private static byte[] is_connectivity_b(double[] arr, int count, int startIdx) //startIdx from 1
    {
        DTrace.in();
       //массив меток вершин
        byte[] marks = new byte[count];
        //пометим первую вершину 2 отметкой
        marks[startIdx - 1] = 2;
        int work = 1;
        //пока есть вершины со второй отметкой
        while (work!=0)
        {
            //ищем вершину со 2 отметкой
            int i=0;
            while (marks[i]!=2)
            {
                i++;
            }
            //нашли, пометим 3 отметкой
            marks[i]=3;
            work--;
            //найдём все соседние вершины с 0 отметкой и пометим их 2 отметкой
            //переберём все вершины, проверим соседство и отметку
            for (int j=0; j<count; j++)
            {
                if (j!=i&&has_connect(arr, count, i+1, j+1)&&marks[j]==0)
                {
                    marks[j]=2;
                    work++;
                }
            }
        }
        return marks;
    }
    
    private static boolean has_connect(double[] arr, int count, int x, int y)
    {
        DTrace.in();
        if (y>x)
        {
            x+=y;
            y=x-y;
            x-=y;
        }
        int offset = (int) ((y-1)*count-(y-1)*(1+(y-2)*0.5)) + (x-y-1);
        boolean ret;
        if (arr[offset]==0)
            ret = false;
        else
            ret = true;
        DTrace.out("return " + ret);
        return ret;
    }
    
    private static double get_connect(double[] arr, int count, int x, int y)
    {
        DTrace.in();
        if (y>x)
        {
            x+=y;
            y=x-y;
            x-=y;
        }
        int offset = (int) ((y-1)*count-(y-1)*(1+(y-2)*0.5)) + (x-y-1);
        DTrace.out();
        return arr[offset];
    }
    
    private static double[] set_connect(double[] arr, int count, int x, int y, double k)
    {
        DTrace.in();
        if (y>x)
        {
            x+=y;
            y=x-y;
            x-=y;
        }
        int offset = (int) ((y-1)*count-(y-1)*(1+(y-2)*0.5)) + (x-y-1);
        arr[offset]=k;
        DTrace.out();
        return arr;
    }
    
    //получение номера вершины по индексу в массиве
    public static int get_y(int count, int index)
    {
        DTrace.in();
        int y = 0;
        
        for (y = 1; y<=count; y++)
        {
            if (index>=((y-1)*count-(y-1)*(1+(y-2)*0.5))&&index<((y-1)*count-(y-1)*(1+(y-2)*0.5))+(count-y))
            {
                return y;
            }
        }
        DTrace.out();
        return y;
    }
    
    //получение номера вершины по индексу в массиве
    public static int get_x(int count, int index)
    {
        DTrace.in();
        int x = 0;
        int y = get_y(count, index);
        x=index-(int)((y-1)*count-(y-1)*(1+(y-2)*0.5))+y+1;
        DTrace.out();
        return x;
    }
    
    private static double get_connectivity(double[] arr, int count)
    {
        DTrace.in();
        double retval = 0;
        
        //граф несвязный
        if (!is_connectivity(arr, count))
        {
            return 0;
        }
        
        //граф из одного ребра и 2х вершин
        if (count==2)
        {
            return arr[0];
        }
        
      //Формула для пятивершинного графа по методу Д.А. Мигова
        if (count == 5)
        {
            double t_a=1-arr[0], 
                   t_b=1-arr[1], 
                   t_c=1-arr[2], 
                   t_d=1-arr[3], 
                   t_e=1-arr[4], 
                   t_f=1-arr[5], 
                   t_g=1-arr[6], 
                   t_h=1-arr[7], 
                   t_u=1-arr[8], 
                   t_v=1-arr[9];
            double k1=1-t_e*(t_f*t_g+t_h*t_u),
                   k2=1-t_h*(t_b*t_u+t_c*t_v),
                   k3=1-t_v*(t_c*t_f+t_d*t_g),
                   k4=1-t_d*(t_a*t_b+t_g*t_u),
                   k5=1-t_a*(t_b*t_c+t_e*t_f),
                   k6=arr[0]*arr[7]*arr[8]+arr[0]*arr[9]*(arr[7]*t_u+t_h*arr[8])+t_a*t_h*t_u*(1-4*t_v),
                   k7=arr[2]*arr[3]*arr[4]+arr[4]*arr[9]*(arr[2]*t_d+t_c*arr[3])+t_d*t_e*t_v,
                   k8=arr[0]*arr[3]*arr[7]+arr[6]*arr[7]*(arr[0]*t_d+t_a*arr[3])+t_a*t_h*t_g,
                   k9=arr[0]*arr[1]*arr[9]+arr[4]*arr[9]*(arr[0]*t_b+t_a*arr[1])+t_a*t_e*t_v,
                   k10=arr[4]*arr[3]*arr[5]+arr[3]*arr[7]*(arr[4]*t_f+t_e*arr[5])+t_d*t_f*t_h,
                   k11=arr[1]*arr[5]*arr[6]+arr[1]*arr[9]*(arr[5]*t_g+t_f*arr[6])+t_b*t_g*t_v,
                   k12=arr[2]*arr[4]*arr[6]+arr[2]*arr[8]*(arr[4]*t_g+t_e*arr[6])+t_c*t_e*t_u,
                   k13=arr[1]*arr[3]*arr[5]+arr[5]*arr[8]*(arr[1]*t_d+t_b*arr[3])+t_b*t_d*t_f,
                   k14=arr[1]*arr[2]*arr[6]+arr[6]*arr[7]*(arr[1]*t_c+t_b*arr[2])+t_b*t_c*t_g,
                   k15=arr[0]*arr[2]*arr[8]+arr[5]*arr[8]*(arr[0]*t_c+t_a*arr[2])+t_c*t_f*t_u;
            return 1-(t_b*t_c*(t_a*t_d*k1+t_f*t_e*(t_d*t_g*k6+t_u*t_v*k8))+
                      t_f*t_g*(t_a*t_e*k2+t_h*t_u*(t_a*t_b*k7+t_c*t_d*k9))+
                      t_b*t_h*(t_e*t_u*k3+t_d*t_v*(t_a*t_f*k12+t_e*t_g*k15))+
                      t_c*t_v*(t_f*t_h*k4+t_a*t_g*(t_b*t_u*k10+t_e*t_h*k13))+
                      t_d*t_u*(t_g*t_v*k5+t_a*t_e*(t_c*t_h*k11+t_f*t_v*k14))
                     );
        }
        
        //Формула для четырёхвершинного графа по методу Д.А. Мигова
        if (count == 4)
        {
            double t_a=1-arr[0], 
                   t_f=1-arr[1], 
                   t_d=1-arr[2], 
                   t_b=1-arr[3], 
                   t_e=1-arr[4], 
                   t_c=1-arr[5];
            return 1-(6*t_a*t_b*t_c*t_d*t_e*t_f+
                      t_a*t_b*t_e+
                      t_a*t_d*t_f+
                      t_b*t_c*t_f+
                      t_c*t_d*t_e-
                      2*(t_b*t_d*t_e*t_f*(t_a+t_c-0.5)+
                         t_a*t_c*t_e*t_f*(t_b+t_d-0.5)+
                         t_a*t_b*t_c*t_d*(t_e+t_f-0.5)
                        )
                     );
        }
        
        //найдём ребро и удалим его 2 способами
        int i = 0;
        while (arr[i]==0)
        {
            i++;
        }
        
        //определяем номера вершин ребра
        int x = get_x(count, i);
        int y = get_y(count, i);
        
        
        //записываем все вершины, с которыми имела связи вершина х
        double[] points = new double[count-1];
        int z = 0;
        for (int j=1; j<=count; j++)
        {
            if (j!=x)
            {
                points[z]=get_connect(arr, count, x, j);
                z++;
            }
        }
        
        //производим удаление вершины x, надо удалить x-ую строку и x-й столбец у меньших индексов из первого графа
        double[] G1_arr = new double[(int) (count*(count-1)*0.5)-(count-1)];
        z = 0;
        for (int k = 0; k<(int) (count*(count-1)*0.5); k++)
        {
            if (get_y(count, k)!=x&&get_x(count, k)!=x)
            {
                G1_arr[z]=arr[k];
                z++;
            }
        }
        
        //восстановим утраченные связи
        for (int j=0; j<count-1; j++)
        {
            if (j+1!=y)
            {
                double p1 = get_connect(G1_arr, count-1, j+1, y);
                p1 = p1+points[j]-p1*points[j];
                G1_arr=set_connect(G1_arr, count-1, j+1, y, p1);
            }
        }
        
        //удалим выбранное ребро во втором графе
        double[] G2_arr=Arrays.copyOf(arr, (int)(count*(count-1)*0.5));
        G2_arr[i]=0;
        
        //вызов рекурсии
        retval = arr[i]*get_connectivity(G1_arr, count-1)+(1-arr[i])*get_connectivity(G2_arr, count);
        DTrace.out();
        return retval;
    }

}
