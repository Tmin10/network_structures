package optalgo.fitnessfunctions;

import optalgo.geneticalgo.Chromosome;
import optalgo.geneticalgo.iFitnessFunctionProvider;

public class ZeroCostFunc implements iFitnessFunctionProvider
{

    @Override
    public double check(Chromosome chromosome)
    {
        // Returns zero to pass all cost limitations
        return 0;
    }

    @Override
    public String toString()
    {
        return "ZeroCost";
    }

}//ok
