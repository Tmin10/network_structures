package optalgo.fitnessfunctions;

import java.util.ArrayList;

import optalgo.geneticalgo.Chromosome;
import optalgo.geneticalgo.iFitnessFunctionProvider;
import optalgo.geneticalgo.edge;

public class StoerWagner implements iFitnessFunctionProvider
{        
    @Override
    public double check(Chromosome chromosome)
    {
        double[][] edges = getEdgesWithProbabilities(chromosome);
        
        double mincut = minCut(edges);
        
        return mincut;
    }
    
    @Override
    public String toString()
    {
        return "StoerWagner";
    }
    
    /****************************************************
     * 
     *              A L G O  M E T H O D S
     *      
     ***************************************************/

    private double[][] getEdgesWithProbabilities(Chromosome c)
    {
        int nodesNum = (int) ((1 + (int) Math.sqrt(1 + 8 * c.length)) / 2);
        double[][] edges = new double[nodesNum][nodesNum];
        int from, to; // from 1 to count
        for (int i = 0; i < c.length; i++)
        {
            from = MooreShannon.get_x(nodesNum, i);
            to = MooreShannon.get_y(nodesNum, i);
            if (c.genotype.get(i))
            {
                edges[from - 1][to - 1] = getEdgeWeight(from, to);
                edges[to - 1][from - 1] = edges[from - 1][to - 1];
            }
            else 
                edges[to - 1][from - 1] = edges[from - 1][to - 1] = 0;
        }
        return edges;
    } //verified
    
    private double getEdgeWeight(int firstNodeIdx, int secondNodeIdx)
    {
        //индексы от единицы
        //TODO cache
        //TODO загрузка матрицы надежностей для всех возможных ребер (now 0.9 hardcoded)
        return 0.2; 
    } //verified
    
    private double[][] cloneMatrix(double[][] matrix)
    {
        double[][] ret = new double[matrix.length][matrix.length];
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix.length; j++)
                ret[i][j] = matrix[i][j];
        
        return ret;
    } //verified
    
    private double minCut(double[][] graph)
    {
        double[][] g = cloneMatrix(graph);
        ArrayList<Integer> V = new ArrayList<Integer>();
        
        for (int i = 0; i < graph.length; i++)
            for (int j = 0; j < graph.length; j++)
                if (g[i][j] > 0)
                {
                    V.add(i);
                    break;
                }
        
        ArrayList<ArrayList<Integer>> setOfVertices = new ArrayList<ArrayList<Integer>>();
        double bestCut = Double.MAX_VALUE;
        int bestCutIndex = -1;
        int index = 0;
        
        while (V.size() > 1)
        {
            double[] cw = new double[1];
            int[] s_t = new int[2];
            
            ArrayList<Integer> A = new ArrayList<Integer>();
            
            minCutPhase(graph, A, cw, s_t);
            
            setOfVertices.add(A);
            if (cw[0] < bestCut)
            {
                bestCut = cw[0];
                bestCutIndex = index;
            }
            
            index++;
            mergeVertices(graph, s_t[0], s_t[1]);
            
            V.clear();
            
            for (int  i = 0; i < graph.length; i++)
                for (int j = 0; j < graph.length; j++)
                    if (graph[i][j] > 0)
                    {
                        V.add(i);
                        break;
                    }
        }
        
        return bestCut;
        
// lines below make us able to know which edges mincut contains
//        ArrayList<Integer> half = setOfVertices.get(bestCutIndex);
//        ArrayList<edge> edges = new ArrayList<edge>();
//        for (int i = 0; i < half.size(); i++)
//        {
//            for (int j = 0; j < graph.length; j++)
//            {
//                if (!half.contains(j) && graph[half.get(i)][j] > 0)
//                {
//                    edges.add(new edge(half.get(i) + 1, j + 1, graph[half.get(i)][j]));
//                }
//            }
//        }
    }
    
    private void minCutPhase(double[][] graph, ArrayList<Integer> oneHalfOfVertices, double[] cw, int[] s_t)
    {
        int a = -1;
        
        for (int i = 0; i < graph.length && a < 0; i++)
            for (int j = 0;  j < graph.length; j++)
                if (graph[i][j] > 0)
                {
                    a = i;
                    break;
                }
        
        ArrayList<Integer> A = new ArrayList<Integer>();
        ArrayList<Integer> V = new ArrayList<Integer>();
        
        for (int i = 0; i < graph.length; i++)
            for (int j = 0; j < graph.length; j++)
                if (graph[i][j] > 0)
                {
                    V.add(i);
                    break;
                }
        
        A.add(a);
        
        boolean AeqV = A.size() == V.size();
        
        while (!AeqV)
        {
            int MTCV = getMTCV(A, graph);
            A.add(MTCV);
            
            if (A.size() != V.size())
            {
                AeqV = false;
            }
            else
            {
                boolean flag = true;
                for (int i = 0; i < V.size() && flag; i++)
                {
                    boolean flag2 = false;
                    for (int j = 0; j < A.size(); j++)
                        if (V.get(i) == A.get(j))
                        {
                            flag2 = true;
                            break;
                        }
                    if (!flag2) flag = false;
                }
                AeqV = flag;
            }
        }
        
        int _s = A.get(A.size() - 2);
        int _t = A.get(A.size() - 1);
        
        double _cutWeight = 1;
        for (int i = 0; i < graph.length; i++)
        {
            if (graph[_t][i] > 0)
                _cutWeight *= ( 1 - graph[_t][i] );
        }
        _cutWeight = 1 - _cutWeight;

        s_t[0] = _s;
        s_t[1] = _t;
        cw[0] = _cutWeight;
        
        oneHalfOfVertices.clear();
        for (int i = 0; i < A.size() - 1; i++)
            oneHalfOfVertices.add(A.get(i));
    } //completed
    
    private int getMTCV(ArrayList<Integer> v, double[][] graph)
    {
        double maxWeight = 0;
        int vIndex = -1;
        
        for (int i = 0; i < graph.length; i++)
        {
            double sumWeight = 0;
            
            if (!v.contains(i))
                for (int j = 0; j < v.size(); j++)
                    sumWeight += graph[i][v.get(j)];
            
            if (sumWeight > maxWeight)
            {
                maxWeight = sumWeight;
                vIndex = i;
            }
        }
        
        return vIndex;
    } //completed
    
    private void mergeVertices(double[][] graph, int a, int b)
    {
        if (a == b) return;
        
        int n = a < b ? a : b; //min
        int x = a > b ? a : b; //max
        
        for (int i = 0; i < graph.length; i++)
        {
            double sum = 1 - ( 1 - graph[n][i] ) * ( 1 - graph[x][i] );
            graph[n][i] = sum;
            graph[x][i] = 0;
            if (i == n || i == x)
            {
                graph[n][i] = 0;
                graph[x][i] = 0;
            }
            graph[i][n] = graph[n][i];
            graph[i][x] = graph[x][i];
        }
    } //completed
}

















