package optalgo.fitnessfunctions;

import optalgo.geneticalgo.Chromosome;
import optalgo.geneticalgo.iFitnessFunctionProvider;
import optalgo.input.City;
import optalgo.input.ReadCoordinates;
import src.faketests.DTrace;
//import tests.DTrace;

public class GraphCost implements iFitnessFunctionProvider
{
    private double distances[][];

    public GraphCost()
    {
        ReadCoordinates rc = new ReadCoordinates();
        City cities[] = rc.ReadFile(0);
        int cities_count = cities.length;
        distances = new double[cities_count][cities_count];
        for (int i=0; i<cities_count; i++)
        {
            for (int j=0; j<cities_count; j++)
            {
                distances[i][j]=calculateTheDistance(cities[i].lat, cities[i].lon, cities[j].lat, cities[j].lon);
            }
        }
    } //verified
    
    @Override
    public String toString()
    {
        return "GraphCost";
    }
    
    @Override
    public double check(Chromosome chromosome)
    {
        DTrace.in();
        
        int connections = chromosome.length; //that's right
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * connections)) / 2);
        
        double distance = 0;
        
        int from, to;
        
        for (int i=0; i<connections; i++)
        {
            if (chromosome.genotype.get(i))
            {
                from = get_x(servers, i);
                to = get_y(servers, i);
                distance+=distances[from][to];
            }
        }
        DTrace.out();
        return distance;
    } //verified
    
    private double calculateTheDistance(double lat1, double lon1, double lat2, double lon2)
    {
        DTrace.in();
        final int EARTH_RADIUS = 6372795;
        
        // перевести координаты в радианы
        lat1 = lat1 * Math.PI / 180;
        lat2 = lat2 * Math.PI / 180;
        lon1 = lon1 * Math.PI / 180;
        lon2 = lon2 * Math.PI / 180;
        
        // косинусы и синусы широт и разницы долгот
        double cl1 = Math.cos(lat1);
        double cl2 = Math.cos(lat2);
        double sl1 = Math.sin(lat1);
        double sl2 = Math.sin(lat2);
        double delta = lon2 - lon1;
        double cdelta = Math.cos(delta);
        double sdelta = Math.sin(delta);
        
        // вычисления длины большого круга
        double y = Math.sqrt(Math.pow(cl2 * sdelta, 2) + Math.pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
        double x = sl1 * sl2 + cl1 * cl2 * cdelta;
     
        double ad = Math.atan2(y, x);
        double dist = ad * EARTH_RADIUS;
        DTrace.out();
        return dist;
    } //ok
    
    private static int get_y(int count, int index)
    {
        DTrace.in();
        int y;
        
        for (y = 1; y<=count; y++)
        {
            if (index>=((y-1)*count-(y-1)*(1+(y-2)*0.5))&&index<((y-1)*count-(y-1)*(1+(y-2)*0.5))+(count-y))
            {
                return y;
            }
        }
        DTrace.out();
        return y;
    } //ok
    
    //получение номера вершины по индексу в массиве
    private static int get_x(int count, int index)
    {
        DTrace.in();
        int x = 0;
        int y = get_y(count, index);
        x=index-(int)((y-1)*count-(y-1)*(1+(y-2)*0.5))+y+1;
        DTrace.out();
        return x;
    } //ok
    
//    @Override
//    public double[][] getCosts()
//    {
//        int len = distances.length;
//        double[][] ret = new double[len][len];
//        for (int i = 0; i < len; i++)
//            for (int j = 0; j < len; j++)
//                ret[i][j] = distances[i][j];
//        return ret;
//    } //verified

}
