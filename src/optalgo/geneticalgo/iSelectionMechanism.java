package optalgo.geneticalgo;

public interface iSelectionMechanism
{
	public boolean init(iItemsProvider obj);

	public Chromosome getFirstParent();
	
	public Chromosome getSecondParent();
}
