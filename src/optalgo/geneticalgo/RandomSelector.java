package optalgo.geneticalgo;

import java.util.Random;

import tests.DTrace;

public class RandomSelector implements iSelectionMechanism
{
	private Chromosome[] items;
	private int itemsCount;
	boolean initialised;

	RandomSelector()
	{
	    DTrace.in();
		DTrace.out();
	} //verified

	public boolean init(iItemsProvider obj)
	{
	    DTrace.in();
		items = obj.getItems();
		itemsCount = obj.getAvailableItemsCount();
		initialised = true;
		DTrace.out();
		return initialised;
	} //verified

	@Override
	public Chromosome getFirstParent()
	{
	    DTrace.in();
		if (!initialised)
		{
		    DTrace.out("Not initialised");
			return null;
		}
		Random generator = new Random();
		int idx = generator.nextInt(itemsCount);
		DTrace.out();
		return items[idx];
	} //verified

	@Override
	public Chromosome getSecondParent()
	{
		return getFirstParent();
	} //verified

} //verified
