package optalgo.geneticalgo;

public class edge
{
    public int fromIdx;
    public int toIdx;
    public double weight;

    public edge(int from, int to)
    {
        fromIdx = from;
        toIdx = to;
        weight = 0xDEADCAFE; // -5.5903565e8
    }
    
    public edge(int from, int to, double weight)
    {
        fromIdx = from;
        toIdx = to;
        this.weight = weight;
    }
}