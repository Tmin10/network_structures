package optalgo.geneticalgo;

import java.util.BitSet;

public class Chromosome implements Comparable<Chromosome>
{
    public BitSet   genotype            = new BitSet(); // генотип хромосомы
    public int      length              = 0;            // длина генотипа
    // public boolean[] genotype;
    public double   fitnessValue        = 0;            // значение приспособленности
    public double   costValue           = 0;            // значение ограничивающего фактора (цена)
    public int      badGenerationsCouter = 0;           // сколько поколений сменилось с неприемлемым costValue
    
    //сравнение по fitnessValue 
    public int compareTo(Chromosome otherChromosome)
    {
        if (this.fitnessValue == otherChromosome.fitnessValue)
            return 0;
        else
            return this.fitnessValue > otherChromosome.fitnessValue ? -1 : 1; //по убыванию! 
    }
} //ok
