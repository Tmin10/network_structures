package optalgo.geneticalgo;

import java.util.ArrayList;

import com.sun.org.apache.bcel.internal.generic.DLOAD;

import tests.DTrace;

public class EliteSelector implements iSelectionMechanism
{
	private Chromosome[] items;
	private int itemsCount;
	ArrayList<Chromosome> usedElements;
	private double maxWeight;
	private int pointer;
	private boolean initialised;
	
	public EliteSelector()
	{
	    DTrace.in();
		initialised = false;
		DTrace.out();
	}

	@Override
	public boolean init(iItemsProvider obj)
	{
	    DTrace.in();
		items = obj.getItems();
		itemsCount = obj.getAvailableItemsCount();
		usedElements = new ArrayList<Chromosome>();
		if (0 == itemsCount)
		{
			initialised = false;
			DTrace.out("Not initialised, itemsCount == 0");
			return initialised;
		}

		refreshPointer();

		initialised = true;
		DTrace.out();
		return initialised;
	}
	
	private void refreshPointer()
	{
	    DTrace.in();
		maxWeight = 0;
		for (int i = 0; i < itemsCount; i++)
			if (!usedElements.contains(items[i]))
				if (items[i].fitnessValue >= maxWeight)
				{
					maxWeight = items[i].fitnessValue;
					pointer = i;
				}
		DTrace.out();
	}

	@Override
	public Chromosome getFirstParent()
	{
	    DTrace.in();
		if (!initialised)
		{
		    DTrace.out("Not initialised");
			return null;
		}

		Chromosome ret = items[pointer];
		if (!usedElements.contains(ret))
			usedElements.add(ret);

		if (usedElements.size() == itemsCount)
		{
		    DTrace.out("All elements already selected");
			return null;
		}
		else
			refreshPointer();

		DTrace.out();
		return ret;
	}

	@Override
	public Chromosome getSecondParent()
	{
		return getFirstParent();
	}

}//ok
