package optalgo.geneticalgo;

public interface iFitnessFunctionProvider
{
	public double check(Chromosome chromosome);

    public String toString();

}