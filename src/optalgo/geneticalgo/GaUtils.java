package optalgo.geneticalgo;

import java.util.BitSet;

import tests.DTrace;

public class GaUtils
{
	public static int getHammingDistance(boolean[] arr1, boolean[] arr2)
	{
	    DTrace.in();
		if (arr1.length != arr2.length)
			return -1;

		int distance = 0;
		for (int i = 0; i < arr1.length; i++)
			if (arr1[i] != arr2[i])
				distance++;

		DTrace.out();
		return distance;
	}//ok
	
	public static int getHammingDistance(Chromosome c1, Chromosome c2)
	{
	    DTrace.in();
	    BitSet c1copy = (BitSet) c1.genotype.clone();
	    c1copy.xor(c2.genotype);
	    DTrace.out();
	    return c1copy.cardinality();
	} //verified
}
