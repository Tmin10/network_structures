package optalgo.geneticalgo;

import java.util.Random;

import tests.DTrace;

public class RouletteSelector implements iSelectionMechanism
{
	private Chromosome[] items;
	private int[] itemsByProbability;
	private boolean initialised;

	public RouletteSelector()
	{
	    DTrace.in();
		DTrace.out();
	} //verified

	@Override
	public boolean init(iItemsProvider obj)
	{
	    DTrace.in();
	    initialised = false;
		items = obj.getItems();
		
		int itemsCount = obj.getAvailableItemsCount();
		
		if (0 == itemsCount)
		{
		    DTrace.out("Not initialised, itemsCount == 0");
			return false;
		}
		
		int totalPlaces = 0;
		for (int i = 0; i < itemsCount; i++) {
			int places = (int) (items[i].fitnessValue * 100);
			totalPlaces += places;
		}
		
		if (0 == totalPlaces)
        {
            DTrace.out("Not initialised, totalPlaces == 0");
            return false;
        }
		
		itemsByProbability = new int[totalPlaces];
		int pointer = 0;

		for (int i = 0; i < itemsCount; i++) {
			int places = (int) (items[i].fitnessValue * 100);
			for (int j = 0; j < places; j++) {
				itemsByProbability[pointer] = i;
				pointer++;
			}
		}
		
		initialised = true;
		DTrace.out();
		return initialised;
	} //verified

	@Override
	public Chromosome getFirstParent()
	{
	    DTrace.in();
		if (!initialised)
		{
		    DTrace.out("Not initialised");
			return null;
		}
		Random generator = new Random();
		int pos = generator.nextInt(itemsByProbability.length);
		int idx = itemsByProbability[pos];
		DTrace.out();
		return items[idx];
	} //verified

	@Override
	public Chromosome getSecondParent()
	{
		return getFirstParent();
	} //verified
} //verified
