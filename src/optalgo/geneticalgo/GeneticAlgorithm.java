package optalgo.geneticalgo;

import java.util.BitSet;
import java.util.Random;
import java.util.ArrayList;

import optalgo.utils.Quick3way;
import jdk.nashorn.internal.runtime.QuotedStringTokenizer;
import optalgo.fitnessfunctions.MooreShannon;
import optalgo.input.CsvStatisticRecorder;
import optalgo.input.iGAStatisticRecorder;
import sun.rmi.runtime.NewThreadAction;
import tests.DTrace;
import tests.PopulationTracer;

public class GeneticAlgorithm implements iItemsProvider
{
    private int m_geneLength; // длина гена
    private int m_populationSize; // размер популяции
    private int m_populationIncrease; // приращение популяции
    private int m_generationsCount; // количество поколений (-1 - не
                                    // используется)
    private double m_mutationProbability; // вероятность мутации
                                          // 0 - нет мутации
                                          // <1 - каждый ген мутирует с
                                          // вероятностью
                                          // m_mutationProbability,
                                          // >1 - в хромосоме подвергнутся
                                          // мутации
                                          // (int)m_mutationProbability генов.
    private iSelectionMechanism m_parentsSelectionMechanism; // тип отбора
                                                             // родителей для
                                                             // скрещивания
    private int m_crossingPositions; // тип кроссинговера 0 - многоточечный,
                                     // 1 - одно-, 2 - двухточечный
    private double m_maxCostValue; // предельное значение стоимости
    private double m_targetFitness; // fitnessValue решения должно быть не
                                    // меньше.
    private int m_leanGenerationsCount;
    private int m_leanGenerationsLimit; // количество отслеживаемых поколений,
                                        // на предмет прекращения получения
                                        // более оптимальных решений
                                        // -1 - этот механизм остановки не
                                        // используется
    private double m_leanGenerationsDelta; // если максимальное решение лучше,
                                           // предыдущего максимального на
                                           // Delta,
                                           // то счетчик leanGenerationsCount -
                                           // сбрасывается

    private int m_maxBadGenerationsForChromosome; // сколько этапов размножения
                                                  // проживет хромосома с
                                                  // недопустимым costValue

    private int m_eliteCount; // доля элитных особей
    private int m_rouletteCount; // доля особей, отобранных по колесу рулетки
    private iFitnessFunctionProvider m_fitFuncProvider; // интерфейс для
                                                        // предоставления
                                                        // целевой функции
    private iFitnessFunctionProvider m_costFuncProvider; // функция ограничения

    private Chromosome[][] m_population; // массив популяции
    private int m_activePopulation; // указывает на используемый массив хромосом
    private int m_populationIdx; // количество занятых позиций в массиве
                                 // популяции
    private Random m_generator;
    private double m_bestOfCurrentPopulation;
    private int m_bestOfCurrentPopulationIdx;
    private double m_bestOfLastGenerations;

    private iGAStatisticRecorder m_statisticRecorder;
    private int m_iterations;

    private double[][] m_costs;
    

    public GeneticAlgorithm(int geneLength,
            int populationSize,
            int populationIncrease,
            int generationsCount,
            double mutationProbability,
            SelectionType parentsSelectionWay,
            int crossingPositions,
            double maxCostValue,
            double targetFitness,
            int leanGenerationLimit,
            double leanGenerationsDelta,
            int maxBadGenerationsForChromosome,
            int eliteCount,
            int rouletteCount,
            iFitnessFunctionProvider fitFuncProvider,
            iFitnessFunctionProvider costFuncProvider,
            iGAStatisticRecorder statisticRecorder)
    {
        DTrace.in();
        m_geneLength = geneLength;
        m_populationSize = populationSize;
        m_populationIncrease = populationIncrease;

        m_iterations = 0;

        if (m_populationIncrease % 2 != 0)
            m_populationIncrease++;

        m_generationsCount = generationsCount;

        if (mutationProbability > 100 || mutationProbability < 0)
        {
            DTrace.log("Incorrect mutationProbability");
            System.exit(-1);
        }

        m_mutationProbability = mutationProbability;
        m_crossingPositions = crossingPositions;

        if (maxCostValue > 1 || maxCostValue < 0)
        {
            DTrace.log("Incorrect maxCostValue " + maxCostValue);
            System.exit(-1);
        }

        m_costFuncProvider = costFuncProvider;
        double fullNetCost =  calculateFullNetCost();
        m_maxCostValue = fullNetCost * maxCostValue;
        DTrace.log("Limit/Full cost = " + ((int)m_maxCostValue) + " / " + ((int)fullNetCost));

        m_costs = getCosts();

        if (targetFitness > 1 || targetFitness < 0)
        {
            DTrace.log("Incorrect targetFitness " + targetFitness);
            System.exit(-1);
        }

        m_targetFitness = targetFitness;
        m_leanGenerationsLimit = leanGenerationLimit;
        m_leanGenerationsCount = leanGenerationLimit;
        m_leanGenerationsDelta = leanGenerationsDelta;
        m_maxBadGenerationsForChromosome = maxBadGenerationsForChromosome;
        m_eliteCount = eliteCount;
        m_rouletteCount = rouletteCount;
        m_fitFuncProvider = fitFuncProvider;

        m_statisticRecorder = statisticRecorder;
        m_generator = new Random();

        m_population = new Chromosome[2][m_populationSize
                + m_populationIncrease];

        m_activePopulation = 0;
        

        // m_pool = new ChromosomesPool(2 * (m_populationSize +
        // m_populationIncrease), m_geneLength); //remove 2x ?

        Chromosome[] population = m_population[m_activePopulation];
        for (int i = 0; i < m_populationSize + m_populationIncrease; i++)
        {
            population[i] = new Chromosome();
            population[i].length = m_geneLength; // todo move to Chromosome
                                                 // constructor

            if (i < m_populationSize)
            {
                initChromosome(population[i]);
                // for (int j = 0; j < m_geneLength; j++)
                // {
                // population[i].genotype.set(j, m_generator.nextBoolean());
                // }
            }
        }
        m_populationIdx = m_populationSize;

        population = m_population[1];
        for (int i = m_populationSize; i < m_populationSize
                + m_populationIncrease; i++)
        {
            population[i] = new Chromosome();
            population[i].length = m_geneLength; // todo move to Chromosome
                                                 // constructor
            population[i].genotype.clear();
        }

        switch (parentsSelectionWay)
        {
        case ST_ROULETTE:
            m_parentsSelectionMechanism = new RouletteSelector();
            break;

        case ST_ELITE:
            m_parentsSelectionMechanism = new EliteSelector();
            break;

        case ST_INBREEDING_RANDOM:
            m_parentsSelectionMechanism = new InbreedingSelector(
                    parentsSelectionWay);
            break;

        case ST_INBREEDING_ROULETTE:
            m_parentsSelectionMechanism = new InbreedingSelector(
                    parentsSelectionWay);
            break;

        case ST_OUTBREEDING_RANDOM:
            m_parentsSelectionMechanism = new OutbreedingSelector(
                    parentsSelectionWay);
            break;

        case ST_OUTBREEDING_ROULETTE:
            m_parentsSelectionMechanism = new OutbreedingSelector(
                    parentsSelectionWay);
            break;

        case ST_RANDOM:
        default:
            m_parentsSelectionMechanism = new RandomSelector();
        }

        fitness(0, m_populationSize);

        if (!m_parentsSelectionMechanism.init(this))
        {
            DTrace.log("m_parentsSelectionMechanism.init = false");
            System.exit(-1);
        }
        DTrace.out();
    }

    @Override
    public Chromosome[] getItems()
    {
        DTrace.in();
        DTrace.out();
        return m_population[m_activePopulation];
    }

    @Override
    public int getAvailableItemsCount()
    {
        DTrace.in();
        DTrace.out();
        return m_populationIdx;
    }
//
//    private void fitness()
//    {
//        fitness(0, m_populationIdx);
//    }

    private void fitness(int lo, int hi)
    {
        DTrace.in();
        if (lo < 0 || hi > m_populationIdx || lo > hi)
        {
            DTrace.log("lo < 0 || hi > m_populationIdx || lo > hi");
            System.exit(-1);
        }
        
        int numCores = Runtime.getRuntime().availableProcessors();
        Chromosome[] population = m_population[m_activePopulation];
        int chunk = (hi - lo) / numCores;
        int rightBound = hi;
        
        ArrayList<Thread> threads = new ArrayList<Thread>();
        for (int i = 1; i <= numCores; i++)
        {
            if (i == numCores)
                rightBound = hi;
            else
                rightBound = lo + chunk;
            
            FitnessTask task = new FitnessTask(lo, rightBound, population, m_fitFuncProvider, m_costFuncProvider);
            lo += chunk;
            
            Thread thread = new Thread(task);
            threads.add(thread);
            thread.start();
        }
        
        for (int i = 0; i < threads.size(); i++)
        {
            try
            {
                threads.get(i).join();
            } catch (InterruptedException e)
            {
                DTrace.log("Fitness task thread was illegally interrupted. Thread = " + i);
                e.printStackTrace();
                System.exit(-1);
            }
        }
        
        threads.clear();
        
//        for (int i = lo; i < hi; i++)
//        {
//            // DTrace.log("iter " + i + " of " + m_populationIdx);
//            if (null == population[i])
//            {
//                DTrace.log("Chromosome with NULL ref inside fitness");
//                System.exit(-1);
//                // continue; //for release version
//            }
//
//            population[i].fitnessValue = m_fitFuncProvider.check(population[i]);
//            population[i].costValue = m_costFuncProvider.check(population[i]);
//        }
        DTrace.out();
    } // 

    private void updateBadGenerationsCounterForChromosomes()
    {
        Chromosome[] population = m_population[m_activePopulation];
        for (int i = 0; i < m_populationSize + m_populationIncrease; i++)
        {
            if (population[i].costValue > m_maxCostValue)
                population[i].badGenerationsCouter++;
            else
                population[i].badGenerationsCouter = 0;

            if (population[i].badGenerationsCouter > m_maxBadGenerationsForChromosome)
            {
                population[i].badGenerationsCouter = 0;
                decreaseChromosomeCost(population[i]);
            }
        }
    }

    private void crossTails(Chromosome c1, Chromosome c2, int position)
    {
        DTrace.in();
        BitSet bs1 = c1.genotype;
        BitSet bs2 = c2.genotype;
        boolean temp;

        for (int i = position; i < c1.length; i++)
        {
            temp = bs1.get(i);
            bs1.set(i, bs2.get(i));
            bs2.set(i, temp);
        }
        DTrace.out();
    } // verified

    private int cross1position(Chromosome parent1, Chromosome parent2,
            Chromosome child1, Chromosome child2)
    {
        DTrace.in();
        for (int i = 0; i < m_geneLength; i++)
        {
            if (null == child1 || null == child2 || null == parent1
                    || null == parent2)
            {
                DTrace.log("NULL parents or child refs");
                System.exit(-1);
            }

            child1.genotype.set(i, parent1.genotype.get(i));
            child2.genotype.set(i, parent2.genotype.get(i));
        }

        int crossPosition = m_generator.nextInt(m_geneLength);
        crossTails(child1, child2, crossPosition);
        DTrace.out();
        return crossPosition;
    } // verified

    private void cross2positions(Chromosome parent1, Chromosome parent2,
            Chromosome child1, Chromosome child2)
    {
        int crossPosition1 = cross1position(parent1, parent2, child1, child2);
        int crossPosition2;
        do
        {
            crossPosition2 = m_generator.nextInt(m_geneLength);
        } while (crossPosition1 == crossPosition2);
        crossTails(child1, child2, crossPosition2);
    } // verified

    private void crossMpositions(Chromosome parent1, Chromosome parent2,
            Chromosome child1, Chromosome child2)
    {
        DTrace.in();
        for (int i = 0; i < m_geneLength; i++)
        {
            child1.genotype.set(i, (i % 2 == 0 ? parent1.genotype.get(i)
                    : parent2.genotype.get(i)));
            child2.genotype.set(i, (i % 2 == 0 ? parent2.genotype.get(i)
                    : parent1.genotype.get(i)));
        }
        DTrace.out();
    } // verified

    private void mutation(Chromosome c, int positionsCount)
    {
        DTrace.in();
        if (positionsCount > m_geneLength)
        {
            DTrace.log("positionsCount > m_geneLength");
            System.exit(-1);
            // positionsCount %= m_geneLength;
        }

        ArrayList<Integer> positions = new ArrayList<Integer>();
        int pos;

        do
        {
            pos = m_generator.nextInt(m_geneLength);
            if (!positions.contains(pos))
                positions.add(pos);
        } while (positions.size() < positionsCount);

        for (int i = 0; i < positionsCount; i++)
        {
            pos = positions.get(i);
            c.genotype.flip(pos);
        }
        DTrace.out();
    } // verified

    private void mutation(Chromosome c, double probability)
    {
        DTrace.in();
        for (int i = 0; i < m_geneLength; i++)
        {
            if (m_generator.nextDouble() < probability)
                c.genotype.flip(i);
        }
        DTrace.out();
    } // verified

    private void crossingAndMutation()
    {
        DTrace.in();
        Chromosome parent1, parent2, child1, child2;

        boolean byGeneMutation = m_mutationProbability < 1 ? true : false;

        if (m_populationIdx != m_populationSize)
        {
            DTrace.log("m_populationIdx != m_populationSize");
            System.exit(-1);
        }
        // m_populationIdx = m_populationSize;

        for (int i = m_populationIncrease; i > 0; i -= 2)
        {
            parent1 = m_parentsSelectionMechanism.getFirstParent();
            parent2 = m_parentsSelectionMechanism.getSecondParent();

            child1 = m_population[m_activePopulation][m_populationIdx];
            m_populationIdx++;
            child2 = m_population[m_activePopulation][m_populationIdx];
            m_populationIdx++;

            switch (m_crossingPositions)
            {
            case 1:
                cross1position(parent1, parent2, child1, child2);
                break;
            case 2:
                cross2positions(parent1, parent2, child1, child2);
                break;
            case 0:
            default:
                crossMpositions(parent1, parent2, child1, child2);
            }

            if (m_mutationProbability > 0)
            {
                if (byGeneMutation)
                {
                    mutation(child1, m_mutationProbability);
                    mutation(child2, m_mutationProbability);
                } else
                {
                    int mutationPositions = (int) Math.round(m_geneLength
                            * m_mutationProbability * 0.01);
                    mutation(child1, mutationPositions);
                    mutation(child2, mutationPositions);
                }
            }

            correctChromosome(child1);
            correctChromosome(child2);
            
            child1.badGenerationsCouter = 0;
            child2.badGenerationsCouter = 0;
        }
        DTrace.out();
    } // verified

    public void run()
    {
        DTrace.in();
        m_bestOfLastGenerations = 0;
        m_bestOfCurrentPopulation = 0;
        while (true)
        {
            if (m_iterations > 0)
            {
                if (!m_parentsSelectionMechanism.init(this))
                {
                    DTrace.log("m_parentsSelectionMechanism.init = false");
                    System.exit(-1);
                }
            }

            crossingAndMutation();
            fitness(m_populationSize, m_populationIdx);

            updateBadGenerationsCounterForChromosomes();

            sortChromosomes();

            shrinkExtendedPopulation();
            
            sortChromosomes();

            updateCurrentBestFitness();
            
            //CsvStatisticRecorder csv = (CsvStatisticRecorder) m_statisticRecorder;
            //csv.setComment("Lean gc = " + m_leanGenerationsCount + " " + m_leanGenerationsLimit);
            
            storeIterationResults();
            PopulationTracer pt = new PopulationTracer(this, "pop_"+m_iterations);

            // проверка условий остановки алгоритма
            if (stopByGenerationsCount() || stopByFitness()
                    || stopByLeanGenerations())
            {
                break;
            }
            updateLastGenerationsBestFitness();

            m_iterations++;
        } // loop end
        DTrace.log("GA END");
        
        
        double maxFitness = m_bestOfCurrentPopulation;
        double maxCost = m_bestOfCurrentPopulationIdx != -1 ? m_population[m_activePopulation][m_bestOfCurrentPopulationIdx].costValue : 0;

        DTrace.log("Fit = " + maxFitness + " Cost = " + maxCost);
        DTrace.log("Generations = " + (m_iterations + 1));
        
        DTrace.out();
    } // TODO verify

    private void storeIterationResults()
    {
        DTrace.in();
        
        double maxFitness = m_bestOfCurrentPopulation;
        double maxCost = m_bestOfCurrentPopulationIdx != -1 ? m_population[m_activePopulation][m_bestOfCurrentPopulationIdx].costValue : 0;

        double averageFitness = 0;
        for (int i = 0; i < m_populationSize; i++)
        {
            averageFitness += m_population[m_activePopulation][i].fitnessValue;
        }
        averageFitness /= m_populationSize;

        // CsvStatisticRecorder csv = (CsvStatisticRecorder)
        // m_statisticRecorder;
        // csv.setComment("\t" + m_bestOfLastGenerations + "\t" +
        // m_leanGenerationsCount);

        m_statisticRecorder.storeIterationResult(m_iterations, maxFitness,
                averageFitness, maxCost, (m_bestOfCurrentPopulationIdx != -1 ? m_population[m_activePopulation][m_bestOfCurrentPopulationIdx].genotype : new BitSet()));
        DTrace.out();
    } // verified

    private void shrinkExtendedPopulation()
    {
        DTrace.in();
        int otherPopulation = (m_activePopulation + 1) % 2;
        int otherPopulationIdx = 0;

        iSelectionMechanism selector;
        int used = 0;

        if (m_eliteCount > 0)
        {
            for (int i = m_eliteCount; i > 0; i--)
            {
                m_population[otherPopulation][otherPopulationIdx] = m_population[m_activePopulation][otherPopulationIdx];
                otherPopulationIdx++;
                used++;
            }
            /*
             * Селектор выпилен для исключения двойной работы по сортировке
             * пузырьком selector = new EliteSelector(); selector.init(this);
             * for (int i = m_eliteCount; i > 0; i--) {
             * m_population[otherPopulation][otherPopulationIdx] =
             * selector.getFirstParent(); otherPopulationIdx++; }
             */
        }

        if (m_rouletteCount > 0)
        {
            selector = new RouletteSelector();
            boolean rouletteReady = selector.init(this);

            for (int i = m_rouletteCount; i > 0 && rouletteReady; i--)
            {
                m_population[otherPopulation][otherPopulationIdx] = selector
                        .getFirstParent();
                otherPopulationIdx++;
                used++;
            }
        }

        if (m_populationSize - used > 0)
        {
            selector = new RandomSelector();
            selector.init(this);
            for (int i = m_populationSize - used; i > 0; i--)
            {
                m_population[otherPopulation][otherPopulationIdx] = selector
                        .getFirstParent();
                otherPopulationIdx++;
            }
        }

        m_activePopulation = otherPopulation;
        m_populationIdx = otherPopulationIdx;
        if (m_populationIdx != m_populationSize)
        {
            DTrace.log("m_populationIdx != m_populationSize");
            System.exit(-1);
        }
        DTrace.out();
    } // verified

    private void sortChromosomes()
    {
        DTrace.in();

        Quick3way.sort(m_population[m_activePopulation]); // TODO trace first
                                                          // run

        DTrace.out();
    }

    private boolean stopByGenerationsCount()
    {
        DTrace.in();
        if (-1 == m_generationsCount)
        {
            DTrace.out("stopByGenerationsCount = false (not used)");
            return false;
        }

        if (1 == m_generationsCount)
        {
            DTrace.log("Limit for num of generations reached, return = true");
            m_generationsCount = 0;
            return true;
        }

        m_generationsCount--;
        DTrace.out("ret = false - continue work");
        return false;
    } // verified

    private boolean stopByFitness()
    {
        DTrace.in();

        if (m_bestOfCurrentPopulation >= m_targetFitness)
        {
            DTrace.log("Target fitness reached. return true");
            return true;
        }

        DTrace.out("Target fitness not reached. return false");
        return false;
    } // verified

    private boolean stopByLeanGenerations()
    {
        DTrace.in();
        if (-1 == m_leanGenerationsLimit)
        {
            DTrace.out("Lean generations limit not used. return false");
            return false;
        }

        if (m_bestOfCurrentPopulation - m_bestOfLastGenerations >= m_leanGenerationsDelta)
        {
            m_leanGenerationsCount = m_leanGenerationsLimit;
        }
        else
        {
            m_leanGenerationsCount--;
        }

        if (0 == m_leanGenerationsCount)
        {
            DTrace.log("Lean generations limit reached. return true");
            return true;
        }

        DTrace.out("Lean generations limit not reached. return false");
        return false;
    } // verified

    private void updateCurrentBestFitness()
    {
        DTrace.in();
        
        Chromosome[] population = m_population[m_activePopulation]; //must be sorted
        m_bestOfCurrentPopulation = 0;
        for (int i = 0; i < m_populationSize; i++)
        {
            if (population[i].fitnessValue >= m_bestOfCurrentPopulation 
                    && population[i].costValue <= m_maxCostValue)
            {
                m_bestOfCurrentPopulation = population[i].fitnessValue;
                m_bestOfCurrentPopulationIdx = i;
                return;
            }
        }
        
        m_bestOfCurrentPopulationIdx = -1;

        DTrace.out();
    } // verified

    private void updateLastGenerationsBestFitness()
    {
        DTrace.in();
        if (m_bestOfCurrentPopulation > m_bestOfLastGenerations)
            m_bestOfLastGenerations = m_bestOfCurrentPopulation;
        DTrace.out();
    } // verified

    private double calculateFullNetCost()
    {
        DTrace.in();
        Chromosome c = new Chromosome();
        c.length = m_geneLength;
        c.genotype.set(0, c.length - 1);
        DTrace.out();
        return m_costFuncProvider.check(c);
    } // verified

    private void initChromosome(Chromosome c)
    {
        ArrayList<Integer> vertices = new ArrayList<Integer>();
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);

        for (int i = 1; i <= servers; i++)
        {
            vertices.add(i);
        }

        int prev = 0, next;
        int nextIdx;
        do
        {
            nextIdx = m_generator.nextInt(vertices.size());
            next = vertices.get(nextIdx);
            vertices.remove(nextIdx);

            if (prev > 0)
            {
                setConnect(c, prev, next);
            }

            prev = next;
        } while (vertices.size() > 0);
    } // verified

    private void setConnect(Chromosome c, int x, int y) // x,y с единицы
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        if (y > x)
        {
            x += y;
            y = x - y;
            x -= y;
        }
        int offset = (int) ((y - 1) * servers - (y - 1) * (1 + (y - 2) * 0.5))
                + (x - y - 1);
        c.genotype.set(offset);
    } // verified

    private double[][] getCosts()
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        double ret[][] = new double[servers][servers];
        Chromosome c = new Chromosome();
        c.length = m_geneLength;
        for (int i = 1; i < servers; i++)
        {
            for (int j = i + 1; j <= servers; j++)
            {
                c.genotype.clear();
                setConnect(c, i, j);
                ret[i - 1][j - 1] = m_costFuncProvider.check(c);
                ret[j - 1][i - 1] = ret[i - 1][j - 1];
            }
            ret[i - 1][i - 1] = 0;
        }
        return ret;
    } // verified

    //дополняет сеть до связной используя ребра минимальной стоимости
    private void correctChromosome(Chromosome c)
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        ArrayList<Integer> vertices = MooreShannon
                .getServersConnectedWith(1, c);
        edge newConnection;

        while (vertices.size() < servers)
        {
            newConnection = getCheapServerIndex(vertices, servers);
            ArrayList<Integer> secondPart = MooreShannon
                    .getServersConnectedWith(newConnection.toIdx, c);
            setConnect(c, newConnection.fromIdx, newConnection.toIdx);
            vertices.addAll(secondPart);
        }
    } // verified
    
    private void decreaseChromosomeCost(Chromosome c)
    {
        if (m_maxCostValue - c.costValue > 1e-7)
            return;
        
        ArrayList<Integer> removedEdgesIdxs = new ArrayList<Integer>();
        int removed;
        
        do
        {
            removed = removeMostExpensiveEdge(c);
            correctChromosome(c);
            c.costValue = m_costFuncProvider.check(c);

            if (m_maxCostValue - c.costValue > 1e-7)
                return;
        } while (!removedEdgesIdxs.contains(removed));
        
        c.genotype.clear();
        initChromosome(c);
        c.costValue = m_costFuncProvider.check(c);
    }
    
    private int removeMostExpensiveEdge(Chromosome c)
    {
        int servers = (int) ((1 + (int) Math.sqrt(1 + 8 * m_geneLength)) / 2);
        int from = 0, to = 0;
        double maxCost = 0;
        int maxCostIdx = -1;
        
        for (int i = 0; i < c.length; i++)
        {
            from = MooreShannon.get_x(servers, i);
            to = MooreShannon.get_y(servers, i);
            
            if (m_costs[from - 1][to - 1] > maxCost)
            {
                maxCost = m_costs[from - 1][to - 1];
                maxCostIdx = i;
            }
        }
        
        c.genotype.clear(maxCostIdx);
        return maxCostIdx;
    }

    public edge getCheapServerIndex(ArrayList<Integer> vertices,
            int serversCount)
    {
        double minCost = Double.MAX_VALUE;
        int fromIdx = 0, toIdx = 0;
        boolean firstIter = true;

        for (int i = 0; i < vertices.size(); i++)
        {
            fromIdx = vertices.get(i);
            for (int j = 1; j <= serversCount; j++)
            {
                if (!vertices.contains(j))
                {
                    if (firstIter)
                    {
                        firstIter = false;
                        minCost = m_costs[j - 1][fromIdx - 1];
                        toIdx = j;
                    } else if (m_costs[j - 1][fromIdx - 1] < minCost)
                    {
                        minCost = m_costs[j - 1][fromIdx - 1];
                        toIdx = j;
                    }
                }
            }
        }

        return new edge(fromIdx, toIdx);
    }
}

class FitnessTask implements Runnable
{
    private int lo;
    private int hi;
    private Chromosome[] population;
    private iFitnessFunctionProvider ffp, cfp;
    
    public FitnessTask(int lo, int hi, Chromosome[] population, iFitnessFunctionProvider ffp,
            iFitnessFunctionProvider cfp)
    {
        this.lo = lo;
        this.hi = hi;
        this.population = population;
        this.ffp = ffp;
        this.cfp = cfp;
    }
    
    @Override
    public void run()
    {
        for (int i = lo; i < hi; i++)
        {
            population[i].fitnessValue = ffp.check(population[i]);
            population[i].costValue = cfp.check(population[i]);
        }
    }
    
}
