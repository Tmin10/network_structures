package optalgo.geneticalgo;

import java.util.ArrayList;
import java.util.Random;

import tests.DTrace;

public class OutbreedingSelector implements iSelectionMechanism
{
    private Chromosome[] items;
    private int itemsCount;
    private int[][] hammingDistance;
    private boolean initialised;
    private Chromosome firstParent;
    private iSelectionMechanism firstParentSelector;

    public OutbreedingSelector(SelectionType firstIndividualSelectionType)
    {
        DTrace.in();
        initialised = false;

        switch (firstIndividualSelectionType)
        {
        case ST_OUTBREEDING_ROULETTE:
        case ST_ROULETTE:
            firstParentSelector = new RouletteSelector();
            break;
        case ST_OUTBREEDING_RANDOM:
        case ST_RANDOM:
        default:
            firstParentSelector = new RandomSelector();
        }
        DTrace.out();
    } //verified

    @Override
    public boolean init(iItemsProvider obj)
    {
        DTrace.in();
        boolean selectorStatus = firstParentSelector.init(obj);

        DTrace.log("firstParentSelector.init = " + selectorStatus);

        initialised = false;

        items = obj.getItems();
        itemsCount = obj.getAvailableItemsCount();
        if (0 == itemsCount)
        {
            DTrace.out("itemsCount == 0");
            return false;
        }

        hammingDistance = new int[itemsCount][itemsCount];

        for (int i = 0; i < itemsCount; i++)
        {
            hammingDistance[i][i] = 0;

            for (int j = 0; j < i; j++)
            {
                int dist = GaUtils.getHammingDistance(items[i], items[j]);
                hammingDistance[i][j] = dist;
                hammingDistance[j][i] = dist;
            }
        }

        initialised = selectorStatus;
        DTrace.out("initialised = " + initialised);
        return initialised;
    } //verified

    @Override
    public Chromosome getFirstParent()
    {
        DTrace.in();
        if (!initialised)
        {
            DTrace.out("Not initialised");
            return null;
        }
        firstParent = firstParentSelector.getFirstParent();
        DTrace.out();
        return firstParent;
    } //verified

    @Override
    public Chromosome getSecondParent()
    {
        DTrace.in();
        if (!initialised || null == firstParent)
        {
            DTrace.log("initialised = " + initialised);
            DTrace.log("null == firstParent is " + (null == firstParent));
            DTrace.out();
            return null;
        }

        int firstParentIdx = 0;

        for (int i = 0; i < itemsCount; i++)
            if (firstParent == items[i])
            {
                firstParentIdx = i;
                break;
            }

        int maxDistance = 0;
        int secondIndividualIdx = 0;
        for (int i = 0; i < hammingDistance[firstParentIdx].length; i++)
        {
            if (firstParentIdx == i)
                continue;

            if (hammingDistance[firstParentIdx][i] > maxDistance)
            {
                maxDistance = hammingDistance[firstParentIdx][i];
                secondIndividualIdx = i;
            }
        }

//        ArrayList<Integer> candidates = new ArrayList<Integer>();
//        for (int i = 0; i < hammingDistance[firstParentIdx].length; i++)
//        {
//            if (firstParentIdx == i)
//                continue;
//
//            if (hammingDistance[firstParentIdx][i] == maxDistance)
//                candidates.add(i);
//        }
//
//        Random generator = new Random();
//        int secondIndividualIdx = candidates.get(generator.nextInt(candidates
//                .size()));

        DTrace.out();
        return items[secondIndividualIdx];
    } //verified

}//verified
