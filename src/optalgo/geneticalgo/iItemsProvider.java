package optalgo.geneticalgo;

public interface iItemsProvider
{
	public Chromosome[] getItems();
	
	public int getAvailableItemsCount();
}