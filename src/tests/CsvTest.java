package tests;

import java.io.IOException;

import optalgo.input.CsvStatisticRecorder;
import optalgo.input.XmlGaLaunchConfigurationLoader;

public class CsvTest
{
    public static String getTypeOfDayWithSwitchStatement(String dayOfWeekArg) {
        String typeOfDay;
        switch (dayOfWeekArg) {
            case "Monday":
                typeOfDay = "Start of work week";
                break;
            case "Tuesday":
            case "Wednesday":
            case "Thursday":
                typeOfDay = "Midweek";
                break;
            case "Friday":
                typeOfDay = "End of work week";
                break;
            case "Saturday":
            case "Sunday":
                typeOfDay = "Weekend";
                break;
            default:
                throw new IllegalArgumentException("Invalid day of the week: " + dayOfWeekArg);
        }
        return typeOfDay;
   }
    
    public static void csv()
    {
        CsvStatisticRecorder csv = new CsvStatisticRecorder("conf01");
        /*csv.storeIterationResult(1, 0.1, 0.22, 0.15);
        csv.storeIterationResult(2, 0.3, 0.23, 0.16);
        csv.storeIterationResult(3, 0.5, 0.24, 0.35);
        csv.storeIterationResult(4, 0.66, 0.25, 6.55);*/
        csv.save();
    }

    public static void xml()
    {
        XmlGaLaunchConfigurationLoader cl = new XmlGaLaunchConfigurationLoader();
        try
        {
            cl.loadXml("set1");
        } catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(cl.getLeanGenerationsDelta());
    }
    


}
