package tests;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sun.org.apache.xerces.internal.impl.dv.xs.DateDV;

public class DTrace
{
    private static DTrace tracer = new DTrace();
    private PrintWriter out;
    private long start;
    
    private DTrace()
    {
        start = new Date().getTime();
        try
        {
            out = new PrintWriter("c:\\ga\\trace.txt");
        } catch (FileNotFoundException e)
        {
            System.err.println("Tracer is not available");
            e.printStackTrace();
        }
        t("TRACE STARTED");
    }
    
    private void t(String message)
    {
        if (null == out)
            return;
        
        long diff = new Date().getTime() - start;
        
        String timeStamp = new SimpleDateFormat("mm:ss:SSS").format(new Date(diff)); //HH:mm:ss:SSS
        Throwable t = new Throwable();
        StackTraceElement[] stack = t.getStackTrace();
        int stackIdx = 1;
        if (stack.length > 2) stackIdx++;
        String place = stack[stackIdx].getClassName() + "." + stack[stackIdx].getMethodName();
        
        String msg = timeStamp + "\t" + place + "\t" + message;
        
        out.println(msg);
        System.out.println("TRACE " + msg);
        out.flush();
        
        
    }
    
    public static void log(String message)
    {
        tracer.t(message);
    }
    
    public static void in()
    {
        //tracer.t("+++");
    }
    
    public static void out()
    {
        //tracer.t("---");
    }
    
    public static void out(String msg)
    {
        //tracer.t("--- " + msg);
    }
}
