package tests;

import optalgo.geneticalgo.SelectionType;
import optalgo.worker.SelectionTypeTask;
import optalgo.worker.doubleTask;
import optalgo.worker.intTask;

public class TaskChainTest
{
    public static void checkTasks()
    {
        intTask it1 = new intTask();
        intTask it2 = new intTask();
        doubleTask dt1 = new doubleTask();
        SelectionTypeTask stt = new SelectionTypeTask();

        it1.connectNext(dt1);
        dt1.connectNext(it2);
        it2.connectNext(stt);
        
        try
        {
            it1.set(1, 7, 2);
            dt1.set(0.16, 45.438, 7.002);
            it2.set(9, 12, 1);
        }
        catch (Exception e)
        {
            System.out.println(e);
            return;
        }
        
        int x = 0, y = 0;
        double z = 0;
        SelectionType st = SelectionType.ST_RANDOM;
        
        System.out.println("Begin: x=" + x + " y=" + y + " z=" + z + " st=" + st);
        
//        for (int i = 1; i <= 7; i++)
//        {
//            for (int j = 9; j <= 23; j++)
//            {
//                x = it1.get();
//                y = it2.get();
//                
//                System.out.println("Process: x=" + x + " y=" + y);
//                
//                it1.tick();
//                if (it1.isEndReached())
//                {
//                    System.out.println("EndReached: x=" + x + " y=" + y);
//                    break;
//                }
//                    
//            }
//        }
        
        do
        {
            x = it1.get();
            z = dt1.get();
            y = it2.get();
            st = stt.get();
            System.out.println("Process: x=" + x + " y=" + y + " z=" + z + " st=" + st);
            it1.tick();
            
        } while (!it1.isEndReached());
        
        System.out.println("End: x=" + x + " y=" + y + " z=" + z);
    }
}
