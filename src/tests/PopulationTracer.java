package tests;

import java.io.PrintWriter;
import java.util.Locale;

import optalgo.geneticalgo.Chromosome;
import optalgo.geneticalgo.iItemsProvider;

public class PopulationTracer
{
    private PrintWriter out;
    
    public PopulationTracer(iItemsProvider obj, String filename)
    {
        try 
        {
            out = new PrintWriter("c:\\ga\\pops\\"+ filename +".txt", "UTF-8");
        } catch(Exception e)
        {
            System.err.println("Tracer is not available");
            e.printStackTrace();
        }
        
        Chromosome[] chroms = obj.getItems();
        int bound = true ? chroms.length : obj.getAvailableItemsCount();
        for (int i = 0; i < bound; i++)
        {
            out.printf(new Locale("ru"), "%1$f;%2$f;%3$d%n", chroms[i].fitnessValue, chroms[i].costValue, chroms[i].badGenerationsCouter);     
        }
        
        out.flush();
        out.close();
    }
    
    
}
