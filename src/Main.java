import java.io.IOException;
import java.util.Random;

import optalgo.fitnessfunctions.NewStoerWagner;
import optalgo.fitnessfunctions.StoerWagner;
import optalgo.geneticalgo.Chromosome;
import optalgo.geneticalgo.GeneticAlgorithm;
import optalgo.input.CsvStatisticRecorder;
import optalgo.input.MySQLStatisticRecorder;
import optalgo.input.XmlGaLaunchConfigurationLoader;
import optalgo.simulatedannealing.SimulatedAnnealingAlgorithm;
import optalgo.worker.doubleTask;
import optalgo.worker.intTask;
import tests.DTrace;

public class Main
{
    public static void main(String[] args) throws Exception
    {
        /*Chromosome cr = new Chromosome();
        cr.length=10;
        cr.genotype.set(0);
        cr.genotype.set(1);
        cr.genotype.set(4);
        cr.genotype.set(7);
        cr.genotype.set(8);
        cr.genotype.set(9);
        for (int i=0; i<cr.genotype.length(); i++)
        {
            if (cr.genotype.get(i))
                System.out.print("1");
            else
                System.out.print("0");
        }
        System.out.println("");
        
        NewStoerWagner nsw = new NewStoerWagner();
        StoerWagner sw = new StoerWagner();
        System.out.println(nsw.check(cr));
        System.out.println(sw.check(cr));*/
        
        DTrace.in();
        
        //String confId = "set3";
        String confId = "annealing";
        
        
        XmlGaLaunchConfigurationLoader xml = new XmlGaLaunchConfigurationLoader();
        try
        {
            xml.loadXml(confId);
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }

        MySQLStatisticRecorder msql = new MySQLStatisticRecorder();
        
        switch (xml.getAlgorithmName())
        {
        case "Simulated annealing Algorithm":
            
            intTask GeneLengthA = new intTask(xml.getGeneLength_from(), xml.getGeneLength_to(), xml.getGeneLength_step());
            intTask GenerationsCountA = new intTask(xml.getGenerationsCount_from(), xml.getGenerationsCount_to(), xml.getGenerationsCount_step());
            doubleTask MaxCostValueA = new doubleTask(xml.getMaxCostValue_from(), xml.getMaxCostValue_to(), xml.getMaxCostValue_step());
            doubleTask TargetFitnessA = new doubleTask(xml.getTargetFitness_from(), xml.getTargetFitness_to(), xml.getTargetFitness_step());
            intTask StartTemperatureA = new intTask(xml.getStartTemperature_from(), xml.getStartTemperature_to(), xml.getStartTemperature_step());
            intTask EndTemperatureA = new intTask(xml.getEndTemperature_from(), xml.getEndTemperature_to(), xml.getEndTemperature_step());
            intTask StepsCountA = new intTask(xml.getStepsCount_from(), xml.getStepsCount_to(), xml.getStepsCount_step());
            
            GeneLengthA.connectNext(GenerationsCountA);
            GenerationsCountA.connectNext(MaxCostValueA);
            MaxCostValueA.connectNext(TargetFitnessA);
            TargetFitnessA.connectNext(StartTemperatureA);
            StartTemperatureA.connectNext(EndTemperatureA);
            EndTemperatureA.connectNext(StepsCountA);
            
            int j=1, iterationsCountA = GeneLengthA.getIterationsCount();
            while(!GeneLengthA.isEndReached())
            {
                DTrace.log("SAA iteration "+j+"/"+iterationsCountA);
                msql.write_SAA_configuration(GeneLengthA.get(), 
                        GenerationsCountA.get(), 
                        MaxCostValueA.get(), 
                        TargetFitnessA.get(), 
                        StartTemperatureA.get(), 
                        EndTemperatureA.get(),
                        StepsCountA.get(),
                        xml.getFitnessFunctionProvider(), 
                        xml.getCostFunctionProvider(), 
                        xml.getCoolingSchedule());
                SimulatedAnnealingAlgorithm ssa = new SimulatedAnnealingAlgorithm(GeneLengthA.get(), 
                        GenerationsCountA.get(), 
                        MaxCostValueA.get(), 
                        TargetFitnessA.get(), 
                        StartTemperatureA.get(), 
                        EndTemperatureA.get(), 
                        StepsCountA.get(), 
                        xml.getFitnessFunctionProvider(), 
                        xml.getCostFunctionProvider(), 
                        msql, 
                        xml.getCoolingSchedule());
                ssa.run();
                GeneLengthA.tick();
                j++;
            }
            
            break;
        case "Genetic Algorithm":
            
            intTask GeneLength = new intTask(xml.getGeneLength_from(), xml.getGeneLength_to(), xml.getGeneLength_step());
            intTask PopulationSize = new intTask(xml.getPopulationSize_from(), xml.getPopulationSize_to(), xml.getPopulationSize_step());
            intTask PopulationIncrease = new intTask(xml.getPopulationIncrease_from(), xml.getPopulationIncrease_to(), xml.getPopulationIncrease_step());
            intTask GenerationsCount = new intTask(xml.getGenerationsCount_from(), xml.getGenerationsCount_to(), xml.getGenerationsCount_step());
            doubleTask MutationProbability = new doubleTask(xml.getMutationProbability_from(), xml.getMutationProbability_to(), xml.getMutationProbability_step());
            intTask CrossingPositions = new intTask(xml.getCrossingPositions_from(), xml.getCrossingPositions_to(), xml.getCrossingPositions_step());
            doubleTask MaxCostValue = new doubleTask(xml.getMaxCostValue_from(), xml.getMaxCostValue_to(), xml.getMaxCostValue_step());
            doubleTask TargetFitness = new doubleTask(xml.getTargetFitness_from(), xml.getTargetFitness_to(), xml.getTargetFitness_step());
            intTask LeanGenerationsLimit = new intTask(xml.getLeanGenerationsLimit_from(), xml.getLeanGenerationsLimit_to(), xml.getLeanGenerationsLimit_step());
            doubleTask LeanGenerationsDelta = new doubleTask(xml.getLeanGenerationsDelta_from(), xml.getLeanGenerationsDelta_to(), xml.getLeanGenerationsDelta_step());
            intTask MaxBadGenerationsForChromosome = new intTask(xml.getMaxBadGenerationsForChromosome_from(), xml.getMaxBadGenerationsForChromosome_to(), xml.getMaxBadGenerationsForChromosome_step());
            intTask EliteCount = new intTask(xml.getEliteCount_from(), xml.getEliteCount_to(), xml.getEliteCount_step());
            intTask RouletteCount = new intTask(xml.getRouletteCount_from(), xml.getRouletteCount_to(), xml.getRouletteCount_step());
            
            GeneLength.connectNext(PopulationSize);
            PopulationSize.connectNext(PopulationIncrease);
            PopulationIncrease.connectNext(GenerationsCount);
            GenerationsCount.connectNext(MutationProbability);
            MutationProbability.connectNext(CrossingPositions);
            CrossingPositions.connectNext(MaxCostValue);
            MaxCostValue.connectNext(TargetFitness);
            TargetFitness.connectNext(LeanGenerationsLimit);
            LeanGenerationsLimit.connectNext(LeanGenerationsDelta);
            LeanGenerationsDelta.connectNext(MaxBadGenerationsForChromosome);
            MaxBadGenerationsForChromosome.connectNext(EliteCount);
            EliteCount.connectNext(RouletteCount);
            
            int i=1, iterationsCount = GeneLength.getIterationsCount();
            while(!GeneLength.isEndReached())
            {
                DTrace.log("GA iteration "+i+"/"+iterationsCount);
                msql.write_GA_configuration(GeneLength.get(),
                        PopulationSize.get(), PopulationIncrease.get(),
                        GenerationsCount.get(), xml.getMutationProbability(),
                        xml.getParentsSelectionWay(), CrossingPositions.get(),
                        MaxCostValue.get(), TargetFitness.get(),
                        LeanGenerationsLimit.get(), LeanGenerationsDelta.get(),
                        MaxBadGenerationsForChromosome.get(), EliteCount.get(),
                        RouletteCount.get(), xml.getFitnessFunctionProvider(),
                        xml.getCostFunctionProvider());

                GeneticAlgorithm ga = new GeneticAlgorithm(GeneLength.get(),
                        PopulationSize.get(), PopulationIncrease.get(),
                        GenerationsCount.get(), xml.getMutationProbability(),
                        xml.getParentsSelectionWay(), CrossingPositions.get(),
                        MaxCostValue.get(), TargetFitness.get(),
                        LeanGenerationsLimit.get(), LeanGenerationsDelta.get(),
                        MaxBadGenerationsForChromosome.get(), EliteCount.get(),
                        RouletteCount.get(), xml.getFitnessFunctionProvider(),
                        xml.getCostFunctionProvider(), msql);
                
                ga.run();
                
                GeneLength.tick();
                i++;
            }
            break;
        }
        
        //CsvStatisticRecorder csv = new CsvStatisticRecorder(confId);
        
        //csv.save();

        DTrace.out();
    }
}