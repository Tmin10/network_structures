<?php
if (isset($_POST['form'])&&($_POST['form']=='one'))
{
	header('Content-Type: application/xml; charset=utf-8');
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<algorithm name=\"Genetic Algorithm\">\r\n";
	echo "\t<launchparam name=\"geneLength\" from=\"".$_POST['geneLength_from']."\" to=\"".$_POST['geneLength_to']."\" step=\"".$_POST['geneLength_step']."\"/>\r\n";
	echo "\t<launchparam name=\"populationSize\" from=\"".$_POST['populationSize_from']."\" to=\"".$_POST['populationSize_to']."\" step=\"".$_POST['populationSize_step']."\"/>\r\n";
	echo "\t<launchparam name=\"populationIncrease\" from=\"".$_POST['populationIncrease_from']."\" to=\"".$_POST['populationIncrease_to']."\" step=\"".$_POST['populationIncrease_step']."\"/>\r\n";
	echo "\t<launchparam name=\"generationsCount\" from=\"".$_POST['generationsCount_from']."\" to=\"".$_POST['generationsCount_to']."\" step=\"".$_POST['generationsCount_step']."\"/>\r\n";
	echo "\t<launchparam name=\"mutationProbability\" from=\"".$_POST['mutationProbability_from']."\" to=\"".$_POST['mutationProbability_to']."\" step=\"".$_POST['mutationProbability_step']."\"/>\r\n";
	echo "\t<launchparam name=\"crossingPositions\" from=\"".$_POST['crossingPositions_from']."\" to=\"".$_POST['crossingPositions_to']."\" step=\"".$_POST['crossingPositions_step']."\"/>\r\n";
	echo "\t<launchparam name=\"maxCostValue\" from=\"".$_POST['maxCostValue_from']."\" to=\"".$_POST['maxCostValue_to']."\" step=\"".$_POST['maxCostValue_step']."\"/>\r\n";
	echo "\t<launchparam name=\"targetFitness\" from=\"".$_POST['targetFitness_from']."\" to=\"".$_POST['targetFitness_to']."\" step=\"".$_POST['targetFitness_step']."\"/>\r\n";
	echo "\t<launchparam name=\"leanGenerationsLimit\" from=\"".$_POST['leanGenerationsLimit_from']."\" to=\"".$_POST['leanGenerationsLimit_to']."\" step=\"".$_POST['leanGenerationsLimit_step']."\"/>\r\n";
	echo "\t<launchparam name=\"leanGenerationsDelta\" from=\"".$_POST['leanGenerationsDelta_from']."\" to=\"".$_POST['leanGenerationsDelta_to']."\" step=\"".$_POST['leanGenerationsDelta_step']."\"/>\r\n";
	echo "\t<launchparam name=\"maxBadGenerationsForChromosome\" from=\"".$_POST['maxBadGenerationsForChromosome_from']."\" to=\"".$_POST['maxBadGenerationsForChromosome_to']."\" step=\"".$_POST['maxBadGenerationsForChromosome_step']."\"/>\r\n";
	echo "\t<launchparam name=\"eliteCount\" from=\"".$_POST['eliteCount_from']."\" to=\"".$_POST['eliteCount_to']."\" step=\"".$_POST['eliteCount_step']."\"/>\r\n";
	echo "\t<launchparam name=\"rouletteCount\" from=\"".$_POST['rouletteCount_from']."\" to=\"".$_POST['rouletteCount_to']."\" step=\"".$_POST['rouletteCount_step']."\"/>\r\n";
	
	echo "\t<launchparam name=\"parentsSelectionMechanism\" set=\"";
	if (isset($_POST['parentsSelectionMechanism']))
	{
		foreach ($_POST['parentsSelectionMechanism'] as $key => $value) 
		{
			if ($key==0)
				echo $value;
			else
				echo ';'.$value;
		}
	}
	echo "\" />\r\n";

	echo "\t<launchparam name=\"fitnessFunctionProvider\" set=\"".$_POST['fitnessFunctionProvider']."\" />\r\n";
	echo "\t<launchparam name=\"costFunctionProvider\" set=\"".$_POST['costFunctionProvider']."\" />\r\n";
	echo "</algorithm>";
	die();
}

if (isset($_POST['form'])&&($_POST['form']=='two'))
{
	header('Content-Type: application/xml; charset=utf-8');
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<algorithm name=\"Genetic Algorithm\">\r\n";
	echo "\t<launchparam name=\"geneLength\" value=\"".$_POST['geneLength']."\" />\r\n";
	echo "\t<launchparam name=\"populationSize\" value=\"".$_POST['populationSize']."\" />\r\n";
	echo "\t<launchparam name=\"populationIncrease\" value=\"".$_POST['populationIncrease']."\" />\r\n";
	echo "\t<launchparam name=\"generationsCount\" value=\"".$_POST['generationsCount']."\" />\r\n";
	echo "\t<launchparam name=\"mutationProbability\" value=\"".$_POST['mutationProbability']."\" />\r\n";
	echo "\t<launchparam name=\"crossingPositions\" value=\"".$_POST['crossingPositions']."\" />\r\n";
	echo "\t<launchparam name=\"maxCostValue\" value=\"".$_POST['maxCostValue']."\" />\r\n";
	echo "\t<launchparam name=\"targetFitness\" value=\"".$_POST['targetFitness']."\" />\r\n";
	echo "\t<launchparam name=\"leanGenerationsLimit\" value=\"".$_POST['leanGenerationsLimit']."\"  />\r\n";
	echo "\t<launchparam name=\"leanGenerationsDelta\" value=\"".$_POST['leanGenerationsDelta']."\"  />\r\n";
	echo "\t<launchparam name=\"maxBadGenerationsForChromosome\" value=\"".$_POST['maxBadGenerationsForChromosome']."\" />\r\n";
	echo "\t<launchparam name=\"eliteCount\" value=\"".$_POST['eliteCount']."\" />\r\n";
	echo "\t<launchparam name=\"rouletteCount\" value=\"".$_POST['rouletteCount']."\" />\r\n";
	
	echo "\t<launchparam name=\"parentsSelectionMechanism\" value=\"";
	if (isset($_POST['parentsSelectionMechanism']))
	{
		foreach ($_POST['parentsSelectionMechanism'] as $key => $value) 
		{
			if ($key==0)
				echo $value;
			else
				echo ';'.$value;
		}
	}
	echo "\" />\r\n";

	echo "\t<launchparam name=\"fitnessFunctionProvider\" value=\"".$_POST['fitnessFunctionProvider']."\" />\r\n";
	echo "\t<launchparam name=\"costFunctionProvider\" value=\"".$_POST['costFunctionProvider']."\" />\r\n";
	echo "</algorithm>";
	die();
}


?>
<html>
	<head>
		<style type="text/css">
			html, body {
				margin: 0;
				padding: 0;
			}
		</style>
	</head>
	<body>
		<table>
		    <tr>
		        <td>
		            <form method="POST">
		                <input type="hidden" name="form" value="one">
		                <table>
		                    <thead>
		                        <tr>
		                            <th>name</th>
		                            <th>from</th>
		                            <th>to</th>
		                            <th>step</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        <tr>
		                            <td>
		                            	<b>geneLength</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="geneLength_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="geneLength_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="geneLength_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>populationSize</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="populationSize_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="populationSize_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="populationSize_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>	
		                            	<b>populationIncrease</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="populationIncrease_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="populationIncrease_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="populationIncrease_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>	
		                            	<b>generationsCount</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="generationsCount_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="generationsCount_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="generationsCount_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>	
		                            	<b>mutationProbability</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="mutationProbability_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="mutationProbability_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="mutationProbability_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>	
		                            	<b>parentsSelectionMechanism</b>
		                            </td>
		                            <td colspan="3">
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_ROULETTE">ROULETTE
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_RANDOM" checked>RANDOM
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_INBREEDING_ROULETTE">INBREEDING_ROULETTE
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_INBREEDING_RANDOM">INBREEDING_RANDOM
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_OUTBREEDING_ROULETTE">OUTBREEDING_ROULETTE
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_OUTBREEDING_RANDOM">OUTBREEDING_RANDOM
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_ELITE">ELITE</td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>crossingPositions</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="crossingPositions_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="crossingPositions_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="crossingPositions_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>maxCostValue</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="maxCostValue_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="maxCostValue_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="maxCostValue_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>targetFitness</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="targetFitness_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="targetFitness_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="targetFitness_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>leanGenerationsLimit</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="leanGenerationsLimit_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="leanGenerationsLimit_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="leanGenerationsLimit_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>leanGenerationsDelta</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="leanGenerationsDelta_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="leanGenerationsDelta_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="leanGenerationsDelta_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>maxBadGenerationsForChromosome</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="maxBadGenerationsForChromosome_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="maxBadGenerationsForChromosome_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="maxBadGenerationsForChromosome_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>eliteCount</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="eliteCount_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="eliteCount_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="eliteCount_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>rouletteCount</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="rouletteCount_from">
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="rouletteCount_to">
		                            </td>
		                            <td>
		                            	<input type="text" value="0" name="rouletteCount_step">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>fitnessFunctionProvider</b>
		                            </td>
		                            <td colspan="3">
		                                <input type="radio" name="fitnessFunctionProvider" value="MooreShannon" checked>MooreShannon</td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>costFunctionProvider</b>
		                            </td>
		                            <td colspan="3">
		                                <input type="radio" name="costFunctionProvider" value="GraphCost" checked>GraphCost</td>
		                        </tr>
		                    </tbody>
		                    <tfoot>
		                        <tr>
		                            <td colspan="4" align="center">
		                                <input type="submit" name="submit" value="Generate XML">
		                            </td>
		                        </tr>
		                    </tfoot>
		                </table>
		            </form>
		        </td>
		        <td>
		            <form method="POST">
		                <input type="hidden" name="form" value="two">
		                <table>
		                    <thead>
		                        <tr>
		                            <th>Name</th>
		                            <th>Value</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        <tr>
		                            <td>
		                            	<b>geneLength</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="geneLength">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>populationSize</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="populationSize">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>populationIncrease</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="populationIncrease">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>generationsCount</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="generationsCount">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>mutationProbability</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="mutationProbability">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>	
		                            	<b>parentsSelectionMechanism</b>
		                            </td>
		                            <td>
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_ROULETTE">ROULETTE
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_RANDOM" checked>RANDOM
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_INBREEDING_ROULETTE">INBREEDING_ROULETTE
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_INBREEDING_RANDOM">INBREEDING_RANDOM
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_OUTBREEDING_ROULETTE">OUTBREEDING_ROULETTE
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_OUTBREEDING_RANDOM">OUTBREEDING_RANDOM
		                                <br />
		                                <input type="radio" name="parentsSelectionMechanism[]" value="ST_ELITE">ELITE</td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>crossingPositions</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="crossingPositions">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>maxCostValue</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="maxCostValue">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>targetFitness</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="targetFitness">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>leanGenerationsLimit</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="leanGenerationsLimit">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>leanGenerationsDelta</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="leanGenerationsDelta">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>maxBadGenerationsForChromosome</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="maxBadGenerationsForChromosome">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>eliteCount</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="eliteCount">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>rouletteCount</b>
		                            </td>
		                            <td>
		                                <input type="text" value="0" name="rouletteCount">
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>fitnessFunctionProvider</b>
		                            </td>
		                            <td>
		                                <input type="radio" name="fitnessFunctionProvider" value="MooreShannon" checked>MooreShannon</td>
		                        </tr>
		                        <tr>
		                            <td>
		                            	<b>costFunctionProvider</b>
		                            </td>
		                            <td>
		                                <input type="radio" name="costFunctionProvider" value="GraphCost" checked>GraphCost</td>
		                        </tr>
		                    </tbody>
		                    <tfoot>
		                        <tr>
		                            <td colspan="2" align="center">
		                                <input type="submit" name="submit" value="Generate XML">
		                            </td>
		                        </tr>
		                    </tfoot>
		                </table>
		            </form>
		        </td>
		    </tr>
		</table>
	</body>
</html>