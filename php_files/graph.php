<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Просмотр результатов</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/styles.css">
		<script src="js/Chart.Core.js"></script>
		<script src="js/Chart.Line.js"></script>
	</head>
	<body>
		<div style="margin: auto; margin-top: 30px; width: 1000px;">
			<canvas id="Chart_1" width="1000px" height="400"></canvas>
			<script>
				var main_data = {
				    datasets: [
				        {
				            label: "Dataset",
				            fillColor: "rgba(240,240,240,0.2)",
				            strokeColor: "rgba(21,145,0,1)",
				            pointColor: "rgba(21,145,0,1)",
				            pointStrokeColor: "#fff",
				            pointHighlightFill: "#000",
				            pointHighlightStroke: "rgba(21,145,0,1)"
				         },
				         {
				            label: "Dataset",
				            fillColor: "rgba(240,240,240,0.2)",
				            strokeColor: "rgba(0,88,146,1)",
				            pointColor: "rgba(0,88,146,1)",
				            pointStrokeColor: "#fff",
				            pointHighlightFill: "#000",
				            pointHighlightStroke: "rgba(0,88,146,1)"
				         }
				    ]
				};
				main_data = JSON.stringify(main_data);

				var data_1 = JSON.parse(main_data);

				<?php
					$handle = fopen("conf01.csv", "r");
					fgetcsv($handle, 1000, ";");
					$data = array();
					while (($row = fgetcsv($handle, 1000, ";")) !== FALSE)
					{
						$data[(int)$row[0]]= array((float)str_replace(',', '.', $row[1]), (float)str_replace(',', '.', $row[2]), (float)str_replace(',', '.', $row[3]));
					}
					$labels='data_1.labels = [';
					$labels1='data_2.labels = [';
					$dataset1='data_1.datasets[0].data = [';
					$dataset2='data_1.datasets[1].data = [';
					$dataset3='data_2.datasets[0].data = [';
					
					foreach ($data as $key => $value) {
						if ($key==1)
						{
							$labels.=$key;
							$labels1.=$key;
							$dataset1.=$value[0];
							$dataset2.=$value[1];
							$dataset3.=$value[2];
						}
						else
						{
							$labels.=','.$key;
							$labels1.=','.$key;
							$dataset1.=','.$value[0];
							$dataset2.=','.$value[1];
							$dataset3.=','.$value[2];
						}
					}
					echo $labels.'];'."\r\n";
					echo $dataset1.'];'."\r\n";
					echo $dataset2.'];'."\r\n";

				?>

				new Chart(document.getElementById("Chart_1").getContext("2d")).Line(data_1, {
					bezierCurve: true
				});
			</script>
			<p><span style="color: #159100">Максимум целевой функции</span><br /><span style="color: #005892">Средняя целевая функция</span></p>
			<canvas id="Chart_2" width="1000px" height="400"></canvas>
			<script>
				var main_data = {
				    datasets: [
				        {
				            label: "Dataset",
				            fillColor: "rgba(240,240,240,0.2)",
				            strokeColor: "rgba(0,88,146,1)",
				            pointColor: "rgba(0,88,146,1)",
				            pointStrokeColor: "#fff",
				            pointHighlightFill: "#000",
				            pointHighlightStroke: "rgba(0,88,146,1)"
				         }
				    ]
				};
				main_data = JSON.stringify(main_data);

				var data_2 = JSON.parse(main_data);
				<?php
					echo $labels1.'];'."\r\n";
					echo $dataset3.'];'."\r\n";
				?>
				new Chart(document.getElementById("Chart_2").getContext("2d")).Line(data_2, {
					bezierCurve: true
				});
			</script>
			<p><span style="color: #005892">Стоимость лучшего решения</span></p>
		</div>
	</body>
</html>