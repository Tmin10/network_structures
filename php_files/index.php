<?php
	$db = new mysqli("localhost", "Qwerty", "Qwerty", "optalgo");
	$db->set_charset("utf8");

	echo '<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Просмотр результатов</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/styles.css">
		<script src="js/Chart.Core.js"></script>
		<script src="js/Chart.Line.js"></script>
	</head>
	<body>'."\r\n";
	
		if (!isset($_GET['conf']))
		{
			$configurations_list = array();
			$parameters_name_array = array();
			$rows = $db->query("SELECT *
								FROM
								(
									SELECT conf_id id, parameter, name, value, type, ''
									FROM configurations_integers
									JOIN parameters_book ON parameters_book.id=configurations_integers.parameter
									UNION ALL
									SELECT conf_id id, parameter, name, value, type, ''
									FROM configurations_doubles
									JOIN parameters_book ON parameters_book.id=configurations_doubles.parameter
									UNION ALL
									SELECT conf_id id, parameter, name, value, type,  CAST(configurations.date_time AS time)
									FROM configurations_strings
									JOIN parameters_book ON parameters_book.id=configurations_strings.parameter
                                    JOIN configurations ON configurations_strings.conf_id=configurations.id
								) AS result
								ORDER BY id, parameter");

			$necessary_information_rows = $db->query("SELECT configuration, count(*) as count  FROM optalgo.solutions GROUP BY configuration ORDER BY count DESC");
			$necessary_information = array();
			while ($necessary_information_row =$necessary_information_rows->fetch_row())
			{
				$necessary_information[$necessary_information_row[0]]=$necessary_information_row[1];
			}

			$dates=array();
			while ($row = $rows->fetch_row())
			{
				$configurations_list[$row[0]][$row[1]]=$row[3];
				$parameters_name_array[$row[1]]=array($row[2], $row[4]);
				if ($row[4]==='s' || isset($dates[$row[0]]))
					$dates[$row[0]]=$row[5];
			}
			echo "\t\t<div class='header'>\r\n".
				 "\t\t\t<div class='container'>\r\n".
				 "\t\t\t\t<h1>Configurations list</h1>\r\n".
				 "\t\t\t</div>\r\n".
				 "\t\t</div>".
				 "\t\t<div class='container'>\r\n".
				 "\t\t\t<table class='table table-striped table-hover configurations-table'>\r\n".
			     "\t\t\t\t<thead>\r\n".
			     "\t\t\t\t\t<tr>\r\n".
			     "\t\t\t\t\t\t<th>#</th>\r\n".
			     "\t\t\t\t\t\t<th>time</th>\r\n".
			     "\t\t\t\t\t\t<th>g</th>\r\n";
			foreach ($parameters_name_array as $parameter) 
			{
				echo "\t\t\t\t\t\t".'<th class="rotate"><div><span>'.$parameter[0].'</span></div></th>'."\r\n";
			}
			echo "\t\t\t\t\t\t</tr>\r\n\t\t\t\t</thead>\r\n\t\t\t\t<tbody>\r\n";
			foreach ($configurations_list as $configuration_id => $configuration_value)
			{
				echo "\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><a href='?conf=".$configuration_id.'\'>conf #'.$configuration_id.'</a></td>'."\r\n";
				echo "<td>".$dates[$configuration_id]."</td>";
				echo "<td>".$necessary_information[$configuration_id]."</td>";
				foreach ($parameters_name_array as $parameter_id => $parameter)
				{
					if (isset($configuration_value[$parameter_id]))
						echo "\t\t\t\t\t\t<td>".$configuration_value[$parameter_id]."</td>\r\n";
					else
						echo "\t\t\t\t\t\t<td></td>\r\n";
				}
				echo "\t\t\t\t\t</tr>\r\n";
			}
			echo "\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t</div>";
		}
		else
		{
			$configurationId=(int)$_GET['conf'];
			
			echo '<div class="header"><div class="container">'."<h1>Configuration $configurationId</h1></div></div>";
			$rows = $db->query("SELECT iteration, best_fitness, best_cost, genotype, average_fitness
							   FROM solutions
							   JOIN genotypes ON genotypes.id=solutions.best_genotype
							   WHERE configuration =".$configurationId.'
							   ORDER BY iteration');
			$labels='[';
			$dataset1='main_data.datasets[0].data = [';
			$dataset2='main_data.datasets[1].data = [';
			$dataset3='main_data.datasets[0].data = [';
			$bitsets=array();
			$first = true;
			while ($row = $rows->fetch_row())
			{
				if ($first)
				{
					$first=false;
					$labels.=$row[0];
					$dataset1.=$row[1];
					$dataset2.=$row[4];
					$dataset3.=$row[2];
				}
				else
				{
					$labels.=','.$row[0];
					$dataset1.=','.$row[1];
					$dataset2.=','.$row[4];
					$dataset3.=','.$row[2];
				}
				$bitsets[$row[0]]=$row[3];
			}


			echo "\r\n\t\t<div style='margin: auto; margin-top: 30px; width: 1000px;'>";
			echo "\r\n\t\t\t<canvas id=\"Chart_1\" width=\"1000px\" height=\"400\"></canvas>";
			
			echo '<script>
				var main_data = {
				    datasets: [
				        {
				            label: "Dataset",
				            fillColor: "rgba(240,240,240,0.2)",
				            strokeColor: "rgba(21,145,0,1)",
				            pointColor: "rgba(21,145,0,1)",
				            pointStrokeColor: "#fff",
				            pointHighlightFill: "#000",
				            pointHighlightStroke: "rgba(21,145,0,1)"
				         },
				         {
				            label: "Dataset",
				            fillColor: "rgba(240,240,240,0.2)",
				            strokeColor: "rgba(0,88,146,1)",
				            pointColor: "rgba(0,88,146,1)",
				            pointStrokeColor: "#fff",
				            pointHighlightFill: "#000",
				            pointHighlightStroke: "rgba(0,88,146,1)"
				         }
				    ]
				};'."\r\n";
			
			echo 'main_data.labels ='.$labels.'];'."\r\n";
			echo $dataset1.'];'."\r\n";
			echo $dataset2.'];'."\r\n";


			echo 'new Chart(document.getElementById("Chart_1").getContext("2d")).Line(main_data, {
					bezierCurve: true,
					scaleBeginAtZero: true
				});
			</script>
			<p>
				<span style="color: #159100">Maximum of the fitness function</span><br />
				<span style="color: #005892">Average of the fitness function</span>
			</p>
			<canvas id="Chart_2" width="1000px" height="400"></canvas>
			<script>
				var main_data = {
				    datasets: [
				        {
				            label: "Dataset",
				            fillColor: "rgba(240,240,240,0.2)",
				            strokeColor: "rgba(0,88,146,1)",
				            pointColor: "rgba(0,88,146,1)",
				            pointStrokeColor: "#fff",
				            pointHighlightFill: "#000",
				            pointHighlightStroke: "rgba(0,88,146,1)"
				         }
				    ]
				};';

			echo 'main_data.labels ='.$labels.'];'."\r\n";
			echo $dataset3.'];'."\r\n";
			echo 'new Chart(document.getElementById("Chart_2").getContext("2d")).Line(main_data, {
					bezierCurve: true,
					scaleBeginAtZero: true
				});
			</script>
			<p><span style="color: #005892">Cost of the best solution</span></p>
			<table class="table"><thead><tr><th>#</th><th>bitset</th></tr></thead><tbody>';

			$rows = $db->query("SELECT value
							   FROM configurations_integers
							   WHERE conf_id =".$configurationId.' AND parameter=1');
			list($geneLength) = $rows->fetch_row();
			$servers = (int) ((1 + (int) sqrt(1 + 8 * $geneLength)) / 2);

			foreach ($bitsets as $key => $value) {
				echo '<tr><td>'.$key.'</td><td><a href="http://xml.tmin10.ru/map.php?count='.$servers.'&bitset='.$value.'">'.$value.'</td></tr>';
			}
			 
			echo '</tbody></table></div>';

		}
	
	
	
	
	
	
	
	
	
	echo "\r\n\t</body>\r\n</html>";
	
	
	
	
	
	
	
	
	
	
	
	
